package com.slyotis.peakq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQInfo;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.main_pager.MainPagerAdapter;
import com.slyotis.peakq.services.PQIdService;
import com.slyotis.peakq.services.PQNotificationsService;
import com.slyotis.peakq.views.SlidingTabLayout;

public class Main extends AppCompatActivity{

    private static final String TAG = Main.class.getName();

    private FirebaseAnalytics firebaseAnalytics;

    private final static String LAST_ACTIVE_PAGE = "active_page";
    private int lastSelectedPage = 1;

    private ViewPager pager;
    private MainPagerAdapter pagerAdapter;
    private SlidingTabLayout pagerTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Retrieve analytics instance
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Defines main pager for navigation
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), this);
        pager = (ViewPager)findViewById(R.id.main_pager);
        pager.setAdapter(pagerAdapter);


        // Initializes the tab bar and connects it to the pager
        pagerTabs = (SlidingTabLayout) findViewById(R.id.main_pager_tabs);
        pagerTabs.setSelectedIndicatorColors(ContextCompat.getColor(this,
                R.color.tabView_selector));
        pagerTabs.setDistributeEvenly(true);
        pagerTabs.setViewPager(pager);

        if(savedInstanceState != null){
            pager.setCurrentItem(savedInstanceState.getInt(LAST_ACTIVE_PAGE, lastSelectedPage));
        } else {
            pager.setCurrentItem(lastSelectedPage);
        }

        Intent idService = new Intent(this, PQIdService.class);
        startService(idService);

        Intent notificationService = new Intent(this, PQNotificationsService.class);
        startService(notificationService);

        updateToken();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(LAST_ACTIVE_PAGE, pager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        lastSelectedPage = pager.getCurrentItem();
        super.onPause();
    }

    private void updateToken(){
        String token = FirebaseInstanceId.getInstance().getToken();
        PQServer.createToken(PQIdService.deviceName,
                token, tokenCallback, this);
    }

    private PQServer.Callback<PQInfo> tokenCallback = new PQServer.Callback<PQInfo>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQInfo response) {
            Log.d(TAG, response.status);
        }
    };

    @Override
    public void onBackPressed() {
        if(pager.getCurrentItem() == 1){
            if(pagerAdapter.getGroupsFragment() != null) {
                if (pagerAdapter.getGroupsFragment().isInGroup()) {
                    pagerAdapter.getGroupsFragment().showGroupsList();
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_login:{
                Intent intent = new Intent(this, Login.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_chat:{
                Intent intent = new Intent(this, Chat.class);
                startActivity(intent);
                return true;
            }
            case R.id.action_empty_db:{
                PQDataHandler.getInstance(this).emptyDatabase();
                return true;
            }
            case android.R.id.home:{
                if(pagerAdapter.getGroupsFragment() != null) {
                    if (pagerAdapter.getGroupsFragment().isInGroup()) {
                        pagerAdapter.getGroupsFragment().showGroupsList();
                    }
                }
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
