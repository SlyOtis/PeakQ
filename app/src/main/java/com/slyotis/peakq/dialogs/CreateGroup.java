package com.slyotis.peakq.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.slyotis.peakq.R;
import com.slyotis.peakq.firebase.FBGroup;
import com.slyotis.peakq.firebase.FBUserGroup;
import com.slyotis.peakq.firebase.PQFire;
import com.slyotis.peakq.views.CreateFragment;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by slyotis on 05.12.16.
 */

public class CreateGroup extends CreateFragment{

    private TextView mTitle;
    private TextView mDescription;
    private ImageView notificationsIcon;
    private TextView notificationsText;
    private boolean getPush = false;

    public static CreateGroup newInstance(){
        return new CreateGroup();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_create_group, container, false);
        v.findViewById(R.id.create_complete).setOnClickListener(this);
        mTitle = (TextView)v.findViewById(R.id.create_title);
        mDescription = (TextView)v.findViewById(R.id.create_description);
        notificationsText = (TextView) v.findViewById(R.id.action_notifications_text);
        notificationsIcon = (ImageView) v.findViewById(R.id.action_notifications_icon);
        v.findViewById(R.id.action_notifications).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_complete: {
                createGroup();
                break;
            }
            case R.id.action_notifications: {
                if(!getPush){
                    notificationsText.setText(R.string.create_group_action_notifications_on);
                    notificationsIcon.setImageResource(R.drawable.ic_notifications_active_white_24dp);
                    getPush = true;
                } else {
                    notificationsText.setText(R.string.create_group_action_notifications_off);
                    notificationsIcon.setImageResource(R.drawable.ic_notifications_off_white_24dp);
                    getPush = false;
                }
                break;
            }
        }
    }

    private void createGroup(){
        String title = mTitle.getText().toString();
        String description = mDescription.getText().toString();
        String created = DateTime.now().toString(PQFire.defaultFormat);

        mTitle.setError(null);
        mDescription.setError(null);

        if(TextUtils.isEmpty(title)){
            mTitle.setError(getString(R.string.create_group_error_missing_title));
            mTitle.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(description)){
            mDescription.setError(getString(R.string.create_group_error_missing_description));
            mDescription.requestFocus();
            return;
        }

        // TODO : ADD more logic
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        String key = ref.child(PQFire.TABLE_GROUPS).push().getKey();

        FBGroup group = new FBGroup(key,
                title,
                description,
                created);

        FBUserGroup userGroup = new FBUserGroup(key, true, getPush, created);


        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(PQFire.TABLE_GROUPS + "/" + key, group);
        childUpdates.put(PQFire.TABLE_USERS + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid()
                + "/" + PQFire.TABLE_GROUPS + "/" + key, userGroup);

        ref.updateChildren(childUpdates);

        dismiss();
    }
}
