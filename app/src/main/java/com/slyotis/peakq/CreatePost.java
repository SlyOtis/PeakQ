package com.slyotis.peakq;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.expirationpicker.ExpirationPickerBuilder;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.main_pager.groups_recycler.GroupsGroups;
import com.slyotis.peakq.main_pager.groups_recycler.GroupsPosts;
import com.toptoche.searchablespinnerlibrary.SearchableListDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.joda.time.format.DateTimePrinter;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by slyotis on 03.11.16.
 */

public class CreatePost extends AppCompatActivity implements CalendarDatePickerDialogFragment.OnDateSetListener{

    private static final String TAG = CreatePost.class.getName();

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    public static final String GROUP_TITLE = "group_title";

    private TextView mGroup;
    private TextView mExpires;
    private TextInputEditText mTitle;
    private TextInputEditText mDescription;
    private ImageView mDisplayImage;

    private LocalDateTime now;
    private LocalDateTime selectedDate;
    private DateTimeFormatter dateFormat;

    private boolean hasImage = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        setSupportActionBar((Toolbar)findViewById(R.id.create_post_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mGroup = (TextView) findViewById(R.id.create_post_group);
        mTitle = (TextInputEditText) findViewById(R.id.create_title);
        mDescription = (TextInputEditText) findViewById(R.id.create_description);
        mExpires = (TextView) findViewById(R.id.action_expires_date);
        mDisplayImage = (ImageView) findViewById(R.id.create_display_image);

        now = DateTime.now().toLocalDateTime();

        mGroup.setText(getIntent().getStringExtra(GROUP_TITLE));
        LocalDateTime next = now.plusWeeks(1);
        dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        selectedDate = next;
        mExpires.setText(dateFormat.print(next));

        findViewById(R.id.action_expires).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthAdapter.CalendarDay minDate = new MonthAdapter.CalendarDay(now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth());
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(CreatePost.this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth())
                        .setDateRange(minDate, null)
                        .setDoneText("Ok")
                        .setCancelText("Cancel");
                cdp.show(getSupportFragmentManager(), TAG);
            }
        });

        findViewById(R.id.action_add_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    /*
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        Log.d(TAG, ex.getMessage());
                        // TODO : Add capture error
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Uri photoURI = FileProvider.getUriForFile(CreatePost.this,
                                "com.slyotis.peakq.provider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }

                    */

                }

            }
        });

        findViewById(R.id.action_select_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: open gallery
            }
        });

        findViewById(R.id.action_add_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: select location
            }
        });

    }

    private String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalCacheDir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.d(TAG, mCurrentPhotoPath);
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mDisplayImage.setImageBitmap(imageBitmap);
            mDisplayImage.setVisibility(View.VISIBLE);
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment calendar, final int year, final int monthOfYear, final int dayOfMonth) {
        RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                .setOnTimeSetListener(new RadialTimePickerDialogFragment.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialTimePickerDialogFragment time, int hourOfDay, int minute) {

                        LocalDateTime newTime = new LocalDateTime(year, monthOfYear + 1, dayOfMonth, hourOfDay, minute);
                        // TODO : Add errror for invalid expires time.
                        mExpires.setText(dateFormat.print(newTime));
                        selectedDate = newTime;
                        Log.d(TAG, selectedDate.toString());
                    }
                })
                .setStartTime(now.getHourOfDay(), now.getMinuteOfHour())
                .setForced24hFormat()
                .setDoneText("Ok")
                .setCancelText("Cancel");
        rtpd.show(getSupportFragmentManager(), TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_post){
            post();
        } else if(item.getItemId() == android.R.id.home){
            setResult(Activity.RESULT_CANCELED);
            finishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    private void post(){
        String title = mTitle.getText().toString();
        String description = mDescription.getText().toString();

        mTitle.setError(null);
        mDescription.setError(null);


        if(TextUtils.isEmpty(title)){
            mTitle.setError(getString(R.string.create_post_error_missing_title));
            mTitle.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(description)){
            mDescription.setError(getString(R.string.create_post_error_missing_description));
            mDescription.requestFocus();
            return;
        }

        Intent data = new Intent();
        data.putExtra(GroupsPosts.CREATE_POST_DATA,
                new PQPost(title, description, hasImage, selectedDate.toDateTime()));
        setResult(Activity.RESULT_OK, data);
        finishAfterTransition();
    }
}
