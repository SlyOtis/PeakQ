package com.slyotis.peakq;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.slyotis.peakq.chat_recycler.ChatArrayAdapter;
import com.slyotis.peakq.custom.SmoothLinearLayoutManager;
import com.slyotis.peakq.data.PQBuilder;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQInfo;
import com.slyotis.peakq.data.PQMembership;
import com.slyotis.peakq.data.PQMessage;
import com.slyotis.peakq.data.PQOffer;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQThread;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.services.PQNotificationsService;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.util.Calendar;

import static com.slyotis.peakq.services.PQNotificationsService.ACTION_DELETE_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.ACTION_NEW_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.CATEGORY_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.MESSAGE_DATA;
import static com.slyotis.peakq.services.PQNotificationsService.ACTION_DELETE_MESSAGE;
import static com.slyotis.peakq.services.PQNotificationsService.ACTION_NEW_MESSAGE;
import static com.slyotis.peakq.services.PQNotificationsService.CATEGORY_MESSAGE;
import static com.slyotis.peakq.services.PQNotificationsService.OFFER_DATA;

public class Chat extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener, CalendarDatePickerDialogFragment.OnDateSetListener{

    private static final String TAG = Chat.class.getName();

    public static final String CHAT_THREAD = "thread";
    public static final String POST_DATA = "post_data";

    private static final int revealDuration = 300;

    private ChatArrayAdapter adapter;
    private RecyclerView recycler;
    private SmoothLinearLayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;

    private EditText mInput;
    private View mInputPanel;
    private ImageView mSend;
    private View mImage;
    private MenuItem mRename;
    private MenuItem mPrivate;

    private View mOfferPanel;
    private View mActionOffer;
    private TextView mOfferRequest;
    private TextView mOfferOffer;
    private EditText mOfferAmount;
    private RelativeLayout chatContainer;
    private View mClose;

    private TextView mExpiresDate;
    private TextView mExpiresMonth;
    private TextView mExpiresYear;

    private int defaultMessageCount = 20;
    private int index = 0;

    private PQThread thread;

    private byte[] imageData = null;

    private boolean offerPanelVisible = false;
    private boolean offerTypeRequest = true;

    private LocalDateTime now;
    private LocalDateTime selectedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        recycler = (RecyclerView)findViewById(R.id.chat_recycler);
        layoutManager = new SmoothLinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recycler.setLayoutManager(layoutManager);
        adapter = new ChatArrayAdapter(this, recycler);
        recycler.setAdapter(adapter);

        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);

        recycler.setLayoutAnimation(new LayoutAnimationController(set, 0.5f));

        chatContainer = (RelativeLayout) findViewById(R.id.chat);

        mOfferPanel = findViewById(R.id.chat_offer_panel);
        mOfferAmount = (EditText)findViewById(R.id.chat_offer_amount);

        mInputPanel = findViewById(R.id.chat_input_panel);

        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.chat_refresh);
        refreshLayout.setOnRefreshListener(this);

        mSend = (ImageView) findViewById(R.id.chat_action_send);
        mSend.setOnClickListener(this);

        mClose = findViewById(R.id.chat_action_close);
        mClose.setOnClickListener(this);

        mInput = (EditText)findViewById(R.id.chat_input);
        mInput.addTextChangedListener(textWatcher);
        mInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && adapter.isDeleteVisible())
                    adapter.hideDelete();
            }
        });

        mExpiresDate = (TextView) findViewById(R.id.chat_offer_expires_date);
        mExpiresMonth = (TextView) findViewById(R.id.chat_offer_expires_month);
        mExpiresYear = (TextView) findViewById(R.id.chat_offer_expires_year);

        now = DateTime.now().toLocalDateTime();

        selectedDate = now.plusWeeks(1);
        mExpiresDate.setText(String.valueOf(selectedDate.getDayOfMonth()));
        mExpiresMonth.setText(selectedDate.toString("MMMM"));
        mExpiresYear.setText(String.valueOf(selectedDate.getYear()));

        if(now.getYear() != selectedDate.getYear()){
            mExpiresYear.setVisibility(View.VISIBLE);
        }else {
            mExpiresYear.setVisibility(View.GONE);
        }

        findViewById(R.id.chat_offer_action_decrease).setOnClickListener(this);
        findViewById(R.id.chat_offer_action_increase).setOnClickListener(this);
        findViewById(R.id.chat_offer_expires_panel).setOnClickListener(this);

        mOfferOffer = (TextView)findViewById(R.id.chat_offer_panel_action_offer);
        mOfferOffer.setOnClickListener(this);

        mOfferRequest = (TextView) findViewById(R.id.chat_offer_panel_action_request);
        mOfferRequest.setOnClickListener(this);

        mImage = findViewById(R.id.chat_action_image);
        mImage.setOnClickListener(this);
        mActionOffer = findViewById(R.id.chat_action_offer);
        mActionOffer.setOnClickListener(this);

        setSupportActionBar((Toolbar)findViewById(R.id.chat_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        thread = (PQThread)getIntent().getSerializableExtra(CHAT_THREAD);
        if(thread != null){
            getSupportActionBar().setTitle(thread.title);
            getSupportActionBar().setSubtitle(thread.postTitle);
            showAdminOptions();
            onRefresh();
            registerReceiver();
        } else {
            PQPost post = (PQPost)getIntent().getSerializableExtra(POST_DATA);
            if(post != null){
                getSupportActionBar().setTitle(post.title);
                getSupportActionBar().setSubtitle(post.authorName);
                PQServer.createThread(post.id, true, true, threadCallback, this);
            } else {
                // TODO : No post error
                finish();
            }
        }
    }

    private void registerReceiver(){
        IntentFilter msgFilter = new IntentFilter();
        msgFilter.addCategory(CATEGORY_MESSAGE);
        msgFilter.addAction(ACTION_DELETE_MESSAGE);
        msgFilter.addAction(ACTION_NEW_MESSAGE);
        registerReceiver(messageReceiver, msgFilter);

        IntentFilter offerFilter = new IntentFilter();
        offerFilter.addCategory(CATEGORY_OFFER);
        offerFilter.addAction(ACTION_DELETE_OFFER);
        offerFilter.addAction(ACTION_NEW_OFFER);
        registerReceiver(offerReceiver, offerFilter);
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(messageReceiver);
            unregisterReceiver(offerReceiver);
        } catch (IllegalArgumentException ex){
            Log.d(TAG, ex.getMessage());
        }
        super.onDestroy();
    }

    private void showAdminOptions(){
        if(mRename != null)mRename.setVisible(true);
        if(mPrivate != null)mPrivate.setVisible(true);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if(adapter.isDeleteVisible()) adapter.hideDelete();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setSendEnabled(mInput.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setSendEnabled(boolean enabled){
        if(enabled){
            mSend.setEnabled(true);
            mSend.setImageResource(R.drawable.chat_btn_send);
        } else {
            mSend.setEnabled(true);
            mSend.setImageResource(R.drawable.chat_btn_send_disabled);
        }

    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        selectedDate = new LocalDateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
        mExpiresDate.setText(String.valueOf(selectedDate.getDayOfMonth()));
        mExpiresMonth.setText(selectedDate.toString("MMMM"));
        mExpiresYear.setText(String.valueOf(selectedDate.getYear()));
        if(selectedDate.getYear() != now.getYear()){
            mExpiresYear.setVisibility(View.VISIBLE);
        } else {
            mExpiresYear.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.chat_offer_expires_panel:{
                LocalDateTime tomorrow = now.plusDays(1);
                MonthAdapter.CalendarDay minDate = new MonthAdapter.CalendarDay(tomorrow.getYear(), tomorrow.getMonthOfYear() - 1, tomorrow.getDayOfMonth());
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(tomorrow.getYear(), tomorrow.getMonthOfYear() - 1, tomorrow.getDayOfMonth())
                        .setDateRange(minDate, null)
                        .setDoneText("Ok")
                        .setCancelText("Cancel");
                cdp.show(getSupportFragmentManager(), TAG);
                break;
            }
            case R.id.chat_action_image:
                // TODO :: Open camera
                Toast.makeText(this, R.string.error_comming_soon, Toast.LENGTH_SHORT).show();
                break;
            case R.id.chat_action_offer: {
                showOfferPanel();
                setSendEnabled(true);
                break;
            }
            case R.id.chat_action_send: {
                if(thread == null) return;
                Log.d(TAG, "Send");
                if(offerPanelVisible){
                    String amount = mOfferAmount.getText().toString();
                    if(!TextUtils.isEmpty(amount)){
                        int current = Integer.valueOf(amount);
                        if(current > 0){
                            PQServer.newOffer(thread.postId,
                                    Integer.valueOf(mOfferAmount.getText().toString()),
                                    offerTypeRequest ? 1: 0,
                                    selectedDate.toString(PQBuilder.defaultFormat), offerCallback, this);

                            hideOfferPanel();
                            break;
                        }
                    }
                    Toast.makeText(this,R.string.error_offer_null, Toast.LENGTH_SHORT).show();
                } else {
                    String body = mInput.getText().toString();
                    if(!TextUtils.isEmpty(body)) {
                        PQServer.newMessage(thread.id, imageData, body, messageCallback, this);
                        mInput.setText(null);
                    }
                }
                break;
            }
            case R.id.chat_action_close: {
                if(offerPanelVisible){
                    hideOfferPanel();
                    setSendEnabled(mInput.getText().toString().length() > 0);
                    break;
                }
                break;
            }
            case R.id.chat_offer_action_increase: {
                String amount = mOfferAmount.getText().toString();
                if(!TextUtils.isEmpty(amount)){
                    int current  = Integer.valueOf(amount) + 1;
                    mOfferAmount.setText(String.valueOf(current));
                } else {
                    mOfferAmount.setText(String.valueOf(1));
                }
                break;
            }
            case R.id.chat_offer_action_decrease: {
                String amount = mOfferAmount.getText().toString();
                if(!TextUtils.isEmpty(amount)){
                    int current = Integer.valueOf(amount);
                    if(current > 0) {
                        current-=1;
                        mOfferAmount.setText(String.valueOf(current));
                    }
                } else {
                    mOfferAmount.setText(String.valueOf(0));
                }
                break;
            }
            case R.id.chat_offer_panel_action_offer: {
                if(offerTypeRequest){
                    offerTypeRequest = false;
                    mOfferRequest.setBackgroundResource(R.color.chat_offer_action_type_normal);
                    mOfferRequest.setTextColor(ContextCompat.getColor(this, R.color.chat_offer_action_type_text_normal));
                    mOfferOffer.setBackgroundResource(R.color.chat_offer_action_type_selected);
                    mOfferOffer.setTextColor(ContextCompat.getColor(this, R.color.chat_offer_action_type_text_selected));
                }
                break;
            }
            case R.id.chat_offer_panel_action_request: {
                if(!offerTypeRequest){
                    offerTypeRequest = true;
                    mOfferRequest.setBackgroundResource(R.color.chat_offer_action_type_selected);
                    mOfferRequest.setTextColor(ContextCompat.getColor(this, R.color.chat_offer_action_type_text_selected));
                    mOfferOffer.setBackgroundResource(R.color.chat_offer_action_type_normal);
                    mOfferOffer.setTextColor(ContextCompat.getColor(this, R.color.chat_offer_action_type_text_normal));
                }
                break;
            }
        }
    }

    private void hideOfferPanel(){
        offerPanelVisible = false;
        TransitionManager.beginDelayedTransition(chatContainer, TransitionInflater.from(this).inflateTransition(R.transition.offer_panel));
        mOfferPanel.setVisibility(View.GONE);

        mActionOffer.setVisibility(View.VISIBLE);
        mClose.setVisibility(View.INVISIBLE);
        mImage.setVisibility(View.VISIBLE);
        mInput.setHint(R.string.chat_input_text_hint);
    }

    private void showOfferPanel(){
        offerPanelVisible = true;
        TransitionManager.beginDelayedTransition(chatContainer, TransitionInflater.from(this).inflateTransition(R.transition.offer_panel));
        mOfferPanel.setVisibility(View.VISIBLE);


        mActionOffer.setVisibility(View.GONE);
        mClose.setVisibility(View.VISIBLE);
        mImage.setVisibility(View.GONE);
        mInput.setHint(R.string.chat_offer_input_text_hint);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat, menu);
        mRename = menu.findItem(R.id.chat_action_rename);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.chat_action_push:{
                if(thread != null) {
                    PQServer.updateMembership(thread.id,
                            item.isChecked(), threadPushCallback, this);
                } else {
                    Toast.makeText(Chat.this, R.string.error_updating_push, Toast.LENGTH_SHORT).show();
                }
                return true;
            }
            case R.id.chat_action_rename:{
                // TODO: add rename fragment
                Toast.makeText(this, "Comming soon", Toast.LENGTH_SHORT).show();

                return true;
            }
            case R.id.chat_action_private:{
                PQServer.updateThread(thread.id, null, item.isChecked(),
                        threadPrivateCallback, this);
            }
            case android.R.id.home:{
                finishAfterTransition();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        loadMessages(defaultMessageCount, index);
    }

    private void loadMessages(int count, int start){
        PQServer.getMessages(count, start, thread.id, messagesCallback, this);
    }

    private PQServer.Callback<PQMessage> messageCallback = new PQServer.Callback<PQMessage>() {
        @Override
        public void onFailure(int errorCode, String message) {
            refreshLayout.setRefreshing(false);
            Log.d(TAG, errorCode + ", " + message);
            imageData = null;
        }

        @Override
        public void onSuccess(int requestId, PQMessage response) {
            refreshLayout.setRefreshing(false);
            adapter.addItem(response);
            recycler.smoothScrollToPosition(adapter.getItemCount());
            imageData = null;
        }
    };
    @Override
    public void onBackPressed() {
        if(adapter.isDeleteVisible()) {
            adapter.hideDelete();
            return;
        }
        if(mOfferPanel.getVisibility() == View.VISIBLE){
            hideOfferPanel();
            return;
        }
        // TODO: Hide soft input keyboard
        super.onBackPressed();
    }

    private PQServer.Callback<PQInfo> threadPrivateCallback = new PQServer.Callback<PQInfo>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(Chat.this, R.string.error_updating_thread_private, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQInfo response) {
            Log.d(TAG, response.title);
        }
    };

    private PQServer.Callback<PQMembership> threadPushCallback = new PQServer.Callback<PQMembership>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(Chat.this, R.string.error_updating_push, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQMembership response) {
            Log.d(TAG, "Updated thread " + response.refId);
        }
    };

    private PQServer.Callback<PQThread> threadCallback = new PQServer.Callback<PQThread>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQThread response) {
            thread = response;
            if(thread.isAdmin)showAdminOptions();
            onRefresh();
            registerReceiver();
        }
    };

    private PQServer.Callback<PQMessage[]> messagesCallback = new PQServer.Callback<PQMessage[]>() {
        @Override
        public void onFailure(int errorCode, String message) {
            refreshLayout.setRefreshing(false);
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQMessage[] response) {
            refreshLayout.setRefreshing(false);
            adapter.appendItems(response);
            index += response.length;
            recycler.smoothScrollToPosition(adapter.getItemCount());
        }
    };

    private PQServer.Callback<PQOffer> offerCallback = new PQServer.Callback<PQOffer>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(Chat.this, R.string.error_offer_failed, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onSuccess(int requestId, PQOffer response) {
            Toast.makeText(Chat.this, R.string.chat_offer_sent_success, Toast.LENGTH_SHORT).show();
            PQServer.newMessage(thread.id, imageData,
                    mInput.getText().toString(), response.id, messageCallback, Chat.this);
            mInput.setText(null);
            hideOfferPanel();
        }
    };

    public BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case PQNotificationsService.ACTION_NEW_MESSAGE:{
                    adapter.addItem((PQMessage) intent.getSerializableExtra(MESSAGE_DATA));
                    recycler.smoothScrollToPosition(adapter.getItemCount());
                    break;
                }
                case PQNotificationsService.ACTION_DELETE_MESSAGE:{
                    int id = intent.getIntExtra(MESSAGE_DATA, 0);
                    if(id != 0) {
                        adapter.removeItemById(id);
                        recycler.smoothScrollToPosition(adapter.getItemCount());
                    }
                    break;
                }
            }
        }
    };

    public BroadcastReceiver offerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case ACTION_NEW_OFFER:{
                    adapter.updateOfferForItem((PQOffer) intent.getSerializableExtra(OFFER_DATA));
                    recycler.smoothScrollToPosition(adapter.getItemCount());
                    break;
                }
                case ACTION_DELETE_OFFER:{
                    int id = intent.getIntExtra(OFFER_DATA, 0);
                    if(id != 0) {
                        adapter.removeItemByOfferId(id);
                        recycler.smoothScrollToPosition(adapter.getItemCount());
                    }
                    break;
                }

            }
        }
    };
}
