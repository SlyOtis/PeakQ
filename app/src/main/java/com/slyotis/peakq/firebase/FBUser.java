package com.slyotis.peakq.firebase;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static com.slyotis.peakq.firebase.PQFire.User.*;

/**
 * Created by slyotis on 05.12.16.
 */

@IgnoreExtraProperties
public class FBUser implements Serializable{

    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String phone;
    public int credits;

    public FBUser() {

    }

    public FBUser(String id, String firstName, String lastName, String email, String phone, int credits) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.credits = credits;
    }

    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> map = new HashMap<>();
        map.put(ID, id);
        map.put(EMAIL, email);
        map.put(FIRST_NAME, firstName);
        map.put(LAST_NAME, lastName);
        map.put(PHONE, phone);
        map.put(CREDITS, credits);
        return map;
    }
}
