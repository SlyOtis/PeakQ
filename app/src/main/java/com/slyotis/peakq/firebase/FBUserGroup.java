package com.slyotis.peakq.firebase;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by slyotis on 07.12.16.
 */

@IgnoreExtraProperties
public class FBUserGroup extends FBGroup implements Serializable{
    public String id;
    public boolean isAdmin;
    public boolean getPush;
    public String added;

    public FBUserGroup() {
    }

    public FBUserGroup(String id, boolean isAdmin, boolean getPush, String added) {
        this.id = id;
        this.isAdmin = isAdmin;
        this.getPush = getPush;
        this.added = added;
    }
}
