package com.slyotis.peakq.firebase;

import com.google.firebase.database.IgnoreExtraProperties;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by slyotis on 05.12.16.
 */
@IgnoreExtraProperties
public class FBGroup implements Serializable{
    public String id;
    public String name;
    public String about;
    public String created;

    public FBGroup() {

    }

    public FBGroup(String id, String name, String about, String created) {
        this.id = id;
        this.name = name;
        this.about = about;
        this.created = created;
    }
}
