package com.slyotis.peakq.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by slyotis on 05.12.16.
 */

public class PQFire {

    public static final DateTimeFormatter defaultFormat =
            DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    private static final String TAG = PQFire.class.getName();

    public static final String TABLE_USERS = "Users";
    public static final String TABLE_GROUPS = "Groups";


    public static class User {
        public static final String OBJECT_NAME = "User";
        public static final String ID = "uui";
        public static final String EMAIL = "email";
        public static final String PHONE = "phone";
        public static final String DISPLAY_NAME = "display_name";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String CREDITS = "credits";
    }
}