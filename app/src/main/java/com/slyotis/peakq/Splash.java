package com.slyotis.peakq;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.slyotis.peakq.data.PQDataHandler;

public class Splash extends AppCompatActivity{

    private static final String TAG = Splash.class.getName();

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                Intent intent;
                if(user != null){
                    Log.d(TAG, "User logged in");
                    intent = new Intent(Splash.this, Main.class);
                    PQDataHandler db = PQDataHandler.getInstance(Splash.this);
                    db.login(db.getCurrentUser());
                } else {
                    Log.d(TAG, "User not logged in");
                    intent = new Intent(Splash.this, Login.class);
                }
                startActivity(intent);
                finish();
            }
        };

        /*

        Intent intent;
        if(PQDataHandler.isLoggedIn(this)){
            intent = new Intent(this, Main.class);
            PQDataHandler db = PQDataHandler.getInstance(this);
            db.login(db.getCurrentUser());
        } else {
            intent = new Intent(this, Login.class);
        }
        startActivity(intent);
        finish();
        */
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mListener);
    }
}
