package com.slyotis.peakq.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.shapes.PathShape;
import android.util.AttributeSet;
import android.view.WindowInsets;
import android.widget.LinearLayout;

/**
 * Created by slyotis on 05.12.16.
 */

public class TopCornerLinear extends LinearLayout {

    private RectF iconRect;
    private Path clipPath;
    private RectF clipRect;
    private Paint paint;

    public TopCornerLinear(Context context) {
        super(context);
        init();
    }

    public TopCornerLinear(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TopCornerLinear(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TopCornerLinear(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        setWillNotDraw(false);
        clipPath = new Path();
        clipRect = new RectF();
        paint = new Paint();
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        clipPath.reset();
        clipRect.set(0, 0, w , h);
        clipPath.addRoundRect(clipRect, 30, 30, Path.Direction.CW);
        clipPath.addRect(0, 120, w, h, Path.Direction.CW);
        clipPath.close();
    }

    @Override
    public void draw(Canvas canvas) {
        int saved = canvas.save();
        canvas.clipPath(clipPath);
        super.draw(canvas);
        canvas.restoreToCount(saved);
    }

}
