package com.slyotis.peakq.views;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v13.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

/**
 * Created by slyotis on 21.09.16.
 */
public class FabulousActionButton extends FloatingActionButton{

    public static final int TRANSLATION_DIST = 500;
    private boolean visible = true;

    public FabulousActionButton(Context context) {
        super(context);
    }

    public FabulousActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FabulousActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) this.getLayoutParams();
        p.setBehavior(new ScrollAwareFAB(getId()));
        this.setLayoutParams(p);
    }

    public class ScrollAwareFAB extends FloatingActionButton.Behavior {

        private int id;
        public ScrollAwareFAB(int id) {
            super();
            this.id = id;
        }

        @Override
        public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout,
                                           FloatingActionButton child, View directTargetChild,
                                           View target, int nestedScrollAxes) {
            return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL ||
                    super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target,
                            nestedScrollAxes);
        }

        @Override
        public void onNestedScroll(CoordinatorLayout coordinatorLayout, final FloatingActionButton child,
                                   View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
            super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed,
                    dyUnconsumed);

            if (dyConsumed > 0 && child.getVisibility() == View.VISIBLE) {
                child.animate()
                        .alpha(0)
                        .setDuration(300)
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                child.setVisibility(INVISIBLE);
                            }
                        }).start();
            } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
                child.animate()
                        .alpha(1)
                        .setDuration(300)
                        .setInterpolator(new AccelerateDecelerateInterpolator())
                        .withStartAction(new Runnable() {
                            @Override
                            public void run() {
                                child.setVisibility(VISIBLE);
                            }
                        }).start();
            }
        }
    }

    @Override
    public void hide() {
        this.animate()
                .translationYBy(TRANSLATION_DIST)
                .setDuration(500)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        FabulousActionButton.this.setVisibility(INVISIBLE);
                    }
                })
                .start();
    }

    @Override
    public void show() {
        this.animate()
                .translationY(0)
                .setDuration(500)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        FabulousActionButton.this.setVisibility(VISIBLE);
                    }
                })
                .start();
    }
}
