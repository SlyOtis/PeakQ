package com.slyotis.peakq.data;

import java.io.Serializable;

/**
 * Created by slyotis on 08.11.16.
 */

public class PQInfo implements Serializable{
    public String status;
    public String title;
    public int id;

    public PQInfo(String status, String title, int id) {
        this.status = status;
        this.title = title;
        this.id = id;
    }
}
