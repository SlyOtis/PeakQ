package com.slyotis.peakq.data;

import android.graphics.Bitmap;
import android.media.Image;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by slyotis on 24.10.16.
 */

public class PQPost implements Serializable {
    public int id;
    public String title;
    public String description;
    public String authorName;
    public boolean solved;
    public int authorId;
    public DateTime added;
    public DateTime expires;
    public int groupId;
    public int imageId;
    public boolean hasImage;

    public PQPost(String title, String description, boolean hasImage, DateTime expires){
        this.title = title;
        this.description = description;
        this.hasImage = hasImage;
        this.expires = expires;
    }

    public PQPost(int id, String title, String description, int authorId, String authorName,
                  DateTime added, int groupId, boolean solved, int imageId, DateTime expires) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.added = added;
        this.groupId = groupId;
        this.authorId = authorId;
        this.hasImage = imageId != 0;
        this.imageId = imageId;
        this.expires = expires;
        this.authorName = authorName;
        this.solved = solved;
    }
}
