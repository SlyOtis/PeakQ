package com.slyotis.peakq.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by slyotis on 26.09.16.
 */
@Deprecated
public class PQDataHandlerOld extends SQLiteOpenHelper{

    // For debugging
    private static final String TAG = PQDataHandlerOld.class.getName();
    // Shared preferences keys
    private static final String LOGGED_IN_ID = "logged_in_id";
    // General database statics
    private static final String DATABASE_NAME = "MyDBName.db";
    private static final int DATABASE_VERSION = 2;
    // Groups table
    private static final String GROUP_TABLE_NAME = "groups";
    private static final String GROUP_COLUMN_ID = "group_id";
    private static final String GROUP_COLUMN_NAME = "group_name";
    private static final String GROUP_COLUMN_ABOUT = "group_about";
    private static final String GROUP_COLUMN_PRIVATE = "group_private";
    private static final String GROUP_COLUMN_MEMBER = "group_member";
    private static final String GROUP_COLUMN_ADMIN = "group_admin";
    private static final String GROUP_COLUMN_PUSH = "group_push";
    // Users table
    private static final String USER_TABLE_NAME = "users";
    private static final String USER_COLUMN_ID = "user_id";
    private static final String USER_COLUMN_FIRSTNAME = "user_firstname";
    private static final String USER_COLUMN_LASTNAME = "user_lastname";
    private static final String USER_COLUMN_EMAIL = "user_email";
    private static final String USER_COLUMN_PASSWORD = "user_password";
    private static final String USER_COLUMN_PHONE = "user_phone";
    private static final String USER_COLUMN_POINTS = "user_points";
    // Messages table
    private static final String MESSAGE_TABLE_NAME = "messages";
    private static final String MESSAGE_COLUMN_ID = "message_id";
    private static final String MESSAGE_COLUMN_ADDED = "message_added";
    private static final String MESSAGE_COLUMN_EXPIRES = "message_expires";
    private static final String MESSAGE_COLUMN_SENDER = "message_sender";
    private static final String MESSAGE_COLUMN_BODY = "message_body";
    private static final String MESSAGE_COLUMN_PHOTO = "message_photo";
    private static final String MESSAGE_COLUMN_RESPONSE = "message_response";
    // Posts table
    private static final String POST_TABLE_NAME = "posts";
    private static final String POST_COLUMN_ID = "post_id";
    private static final String POST_COLUMN_ADDED = "post_added";
    private static final String POST_COLUMN_EXPIRES = "post_expires";
    private static final String POST_COLUMN_AUTHOR = "post_author";
    private static final String POST_COLUMN_AUTHOR_ID = "post_author_id";
    private static final String POST_COLUMN_BODY = "post_body";
    private static final String POST_COLUMN_HAS_PHOTO = "post_has_photo";
    private static final String POST_COLUMN_GROUP = "post_group";
    // Images table
    private static final String IMAGE_TABLE_NAME = "images";
    private static final String IMAGE_COLUMN_ID = "image_id";
    private static final String IMAGE_COLUMN_IMAGE = "image_image";
    // Other
    private static PQDataHandlerOld instance;
    private int currentUserID;
    private Context context;

    public static PQDataHandlerOld getInstance(Context context){
        if(instance != null){
            return instance;
        } else {
            return instance = new PQDataHandlerOld(context);
        }
    }

    public static boolean isLoggedIn(Context context){
        return getInstance(context).isLoggedIn();
    }

    public static PQUser getCurrentUser(Context context){
        return getInstance(context).getCurrentUser();
    }

    public PQDataHandlerOld(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        this.context = context;
        this.currentUserID = -1;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + GROUP_TABLE_NAME + "("
                + GROUP_COLUMN_ID + " int, "
                + GROUP_COLUMN_NAME + " tinytext, "
                + GROUP_COLUMN_ABOUT + " text, "
                + GROUP_COLUMN_PRIVATE + " int, "
                + GROUP_COLUMN_MEMBER + " int, "
                + GROUP_COLUMN_ADMIN + " int, "
                + GROUP_COLUMN_PUSH + " int);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + USER_TABLE_NAME + "("
                + USER_COLUMN_ID + " int, "
                + USER_COLUMN_FIRSTNAME + " tinytext, "
                + USER_COLUMN_LASTNAME + " tinytext, "
                + USER_COLUMN_EMAIL + " tinytext, "
                + USER_COLUMN_PHONE + " tinytext, "
                + USER_COLUMN_PASSWORD + " tinytext, "
                + USER_COLUMN_POINTS + " int);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + MESSAGE_TABLE_NAME + "("
                + MESSAGE_COLUMN_ID + " int, "
                + MESSAGE_COLUMN_ADDED + " timestamp, "
                + MESSAGE_COLUMN_EXPIRES + " timestamp, "
                + MESSAGE_COLUMN_SENDER + " int, "
                + MESSAGE_COLUMN_BODY + " text, "
                + MESSAGE_COLUMN_PHOTO + " mediumblob, "
                + MESSAGE_COLUMN_RESPONSE + " int);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + POST_TABLE_NAME + "("
                + POST_COLUMN_ID + " int, "
                + POST_COLUMN_ADDED + " timestamp, "
                + POST_COLUMN_EXPIRES + " timestamp, "
                + POST_COLUMN_AUTHOR + " text, "
                + POST_COLUMN_AUTHOR_ID + " int, "
                + POST_COLUMN_BODY + " text, "
                + POST_COLUMN_HAS_PHOTO + " int,"
                + POST_COLUMN_GROUP + " int);");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + IMAGE_TABLE_NAME + "("
                + IMAGE_COLUMN_ID + " int, "
                + IMAGE_COLUMN_IMAGE + " mediumblob);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + GROUP_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + POST_TABLE_NAME);
        onCreate(db);
    }

    public PQUser addUser(String data, String password){
        PQUser user = initUserFromData(data, password);
        return addUser(user);
    }

    public PQUser addUser(PQUser user){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(USER_COLUMN_ID, user.id);
        v.put(USER_COLUMN_FIRSTNAME, user.firstName);
        v.put(USER_COLUMN_LASTNAME, user.lastName);
        v.put(USER_COLUMN_PHONE, user.phone);
        v.put(USER_COLUMN_EMAIL, user.email);
        v.put(USER_COLUMN_PASSWORD, user.password);
        v.put(USER_COLUMN_POINTS, user.credits);
        db.insert(USER_TABLE_NAME, null, v);
        Log.d(TAG, "User: " + user.id + " created");
        return user;
    }

    public PQUser addUserIfNotExist(String data, String password){
        PQUser user = initUserFromData(data, password);
        if(checkDataExists(USER_TABLE_NAME, USER_COLUMN_ID, String.valueOf(user.id))){
            return getUser(user.id);
        } else {
            return addUser(user);
        }
    }


    public PQUser getUser(int id){
        SQLiteDatabase db = getReadableDatabase();

        String selectQuery = "SELECT * FROM " + USER_TABLE_NAME +" WHERE " + USER_COLUMN_ID + "=?";
        Cursor c = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });
        PQUser user = null;
        if (c.moveToFirst()) {
            user = new PQUser(
                    c.getInt(c.getColumnIndex(USER_COLUMN_ID)),
                    c.getString(c.getColumnIndex(USER_COLUMN_FIRSTNAME)),
                    c.getString(c.getColumnIndex(USER_COLUMN_LASTNAME)),
                    c.getString(c.getColumnIndex(USER_COLUMN_EMAIL)),
                    c.getString(c.getColumnIndex(USER_COLUMN_PHONE)),
                    c.getInt(c.getColumnIndex(USER_COLUMN_POINTS)),
                    c.getString(c.getColumnIndex(USER_COLUMN_PASSWORD)));
        }

        c.close();
        return user;
    }

    public PQUser getCurrentUser(){
        return getUser(currentUserID);
    }

    public void login(int userID){
        SharedPreferences.Editor editor = PreferenceManager.
                getDefaultSharedPreferences(context).edit();
        currentUserID = userID;
        editor.putInt(LOGGED_IN_ID, currentUserID);
        editor.apply();
    }

    public void logout(){
        SharedPreferences.Editor editor = PreferenceManager.
                getDefaultSharedPreferences(context).edit();
        currentUserID = -1;
        editor.putInt(LOGGED_IN_ID, currentUserID);
        editor.apply();
    }

    public boolean isLoggedIn(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        currentUserID = pref.getInt(LOGGED_IN_ID, -1);
        return currentUserID > 0;
    }

    public ArrayList<PQGroup> addGroups(String data){
        ArrayList<PQGroup> groups = new ArrayList<>();
        for(String group_data: data.split("\\\\")){
            addGroup(group_data);
        }
        return groups;
    }

    public ArrayList<PQGroup> addGroupsIfNotExists(String data){
        ArrayList<PQGroup> groups = new ArrayList<>();
        for(String group_data: data.split("\\\\")){
            groups.add(addGroupIfNotExists(group_data));
        }
        return groups;
    }

    public PQGroup addGroupIfNotExists(String data){
        String[] attrs = data.split("\\|");

        int id = Integer.parseInt(attrs[0]);
        if(checkDataExists(GROUP_TABLE_NAME, GROUP_COLUMN_ID, String.valueOf(id))){
            return getGroup(id);
        } else {
           //return addGroup(new PQGroup(id, attrs[1], attrs[2], false, false, false));
            return null;
        }
    }

    public PQGroup addGroup(String data){
        String[] attrs = data.split("\\|");
        /*
        PQGroup post = new PQGroup(Integer.parseInt(attrs[0]), attrs[1], attrs[2],
                false, false, false);
                */
        return null;
    }

    public PQGroup addGroup(PQGroup group){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(GROUP_COLUMN_ID, group.id);
        v.put(GROUP_COLUMN_NAME, group.name);
        v.put(GROUP_COLUMN_ABOUT, group.about);
        v.put(GROUP_COLUMN_PRIVATE, group.isPrivate);
        v.put(GROUP_COLUMN_MEMBER, group.isMember);
        v.put(GROUP_COLUMN_PUSH, group.getPush);
        db.insert(GROUP_TABLE_NAME, null, v);
        Log.d(TAG, "Group: " + group.id + " created");
        return group;
    }

    public PQGroup updateGroup(PQGroup group){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(GROUP_COLUMN_ID, group.id);
        v.put(GROUP_COLUMN_NAME, group.name);
        v.put(GROUP_COLUMN_ABOUT, group.about);
        v.put(GROUP_COLUMN_PRIVATE, group.isPrivate);
        v.put(GROUP_COLUMN_MEMBER, group.isMember);
        v.put(GROUP_COLUMN_PUSH, group.getPush);
        db.update(GROUP_TABLE_NAME, v, GROUP_COLUMN_ID + "=" + group.id, null);
        Log.d(TAG, "Group: " + group.id + " updated");
        return group;
    }

    public PQGroup getGroup(int id){
        SQLiteDatabase db = getReadableDatabase();

        String selectQuery = "SELECT * FROM " + GROUP_TABLE_NAME +" WHERE " + GROUP_COLUMN_ID + "=?";
        Cursor c = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });
        PQGroup group = null;
        if (c.moveToFirst()) {
            group = getGroupFromRow(c);
        }
        c.close();
        return group;
    }

    public PQGroup getGroupFromRow(Cursor cursor){
        /*
        return new PQGroup(
                cursor.getInt(cursor.getColumnIndex(GROUP_COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(GROUP_COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(GROUP_COLUMN_ABOUT)),
                cursor.getInt(cursor.getColumnIndex(GROUP_COLUMN_PRIVATE)) > 0,
                cursor.getInt(cursor.getColumnIndex(GROUP_COLUMN_MEMBER)) > 0,
                cursor.getInt(cursor.getColumnIndex(GROUP_COLUMN_PUSH)) > 0);
                */
        return null;
    }

    public ArrayList<PQGroup> updateMemberships(String data){
        ArrayList<PQGroup> groups = new ArrayList<>();
        String[] group_data = data.split("\\\\");
        for(int i = 0; i < group_data.length - 1; i++){
            Log.d("Doing gods work on: ", group_data[i]);
            groups.add(updateMembership(group_data[i]));
        }
        return groups;
    }

    public PQGroup updateMembership(String data){
        SQLiteDatabase db = getWritableDatabase();
        data = data.replace("\\","");
        String[] attrs = data.split("\\|");
        int id = Integer.parseInt(attrs[0]);

        /// 1 = medlem, 2 = varsler
        String query = "UPDATE " + GROUP_TABLE_NAME + " SET "
                + GROUP_COLUMN_MEMBER + " = " + attrs[1] + ", "
                + GROUP_COLUMN_PUSH + " = " + attrs[2] + " WHERE "
                + GROUP_COLUMN_ID + " = " + id;
        db.execSQL(query);

        return getGroup(id);
    }


    private boolean checkDataExists(String TableName, String dbfield, String fieldValue) {
        SQLiteDatabase db = getReadableDatabase();
        String Query = "SELECT * FROM " + TableName + " WHERE " + dbfield + " = " + fieldValue;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public PQUser initUserFromData(String data, String password){
        String[] attrs = data.split("\\|");
        PQUser user = new PQUser(Integer.parseInt(attrs[0]), attrs[1],  attrs[2], attrs[3],
                attrs[4], Integer.parseInt(attrs[5]), password);
        return user;
    }

    public Cursor getGroupsCursor(boolean isMember, @Nullable String searchText){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + GROUP_TABLE_NAME;
        if(isMember) query += " WHERE " + GROUP_COLUMN_MEMBER + " = 1";
        if(searchText != null){
            query += " AND " + GROUP_COLUMN_NAME + " LIKE '%" + searchText + "%'";
        }
        return db.rawQuery(query, null);
    }

    public ArrayList<PQPost> addPostsIfNotExists(String data){
        ArrayList<PQPost> posts = new ArrayList<>();
        for(String post_data: data.split("\\\\")){
            posts.add(addPostIfNotExists(post_data));
        }
        return posts;
    }

    public PQPost addPostIfNotExists(String data){
        String[] attrs = data.split("\\|");
        int id = Integer.parseInt(attrs[0]);

        if(checkDataExists(POST_TABLE_NAME, POST_COLUMN_ID, String.valueOf(id))){
            return getPost(id);
        } else {

            boolean hasPhoto = false;

            try{
                hasPhoto = Integer.parseInt(attrs[2]) > 0;
            }catch (Exception ex){
                Log.d(TAG, ex.getMessage());
            }

            String tmp = attrs[6];
            if(tmp.contains("<")) tmp = tmp.replace("<", "");
            if(tmp.contains(">")) tmp = tmp.replace(">", "");
            int groupId = Integer.parseInt(tmp);

            /*
            PQPost post = new PQPost(id, attrs[1], hasImage, attrs[3], Integer.parseInt(attrs[4]),
                    attrs[5], groupId);
            return addPost(post);
            */
            return null;
        }
    }

    public PQPost addPost(PQPost post){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(POST_COLUMN_ID, post.id);
        v.put(POST_COLUMN_AUTHOR, post.authorId);
        v.put(POST_COLUMN_AUTHOR_ID, post.authorId);
        v.put(POST_COLUMN_ADDED, post.added.toString());
        v.put(POST_COLUMN_BODY, post.title);
        v.put(POST_COLUMN_EXPIRES, "not set yet");
        v.put(POST_COLUMN_HAS_PHOTO, post.hasImage ? 1 : 0);
        v.put(POST_COLUMN_GROUP, post.groupId);
        db.insert(POST_TABLE_NAME, null, v);
        Log.d(TAG, "Group: " + post.id + " created");
        return post;
    }

    public PQPost getPost(int id){
        SQLiteDatabase db = getReadableDatabase();

        String selectQuery = "SELECT * FROM " + POST_TABLE_NAME +" WHERE " + POST_COLUMN_ID + "=?";
        Cursor c = db.rawQuery(selectQuery, new String[] { String.valueOf(id) });
        PQPost post = null;
        if (c.moveToFirst()) {
            post = getPostFromRow(c);
        }
        c.close();
        return post;
    }

    public PQPost getPostFromRow(Cursor cursor){
        /*
        return new PQPost(
                cursor.getInt(cursor.getColumnIndex(POST_COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(POST_COLUMN_BODY)),
                cursor.getInt(cursor.getColumnIndex(POST_COLUMN_HAS_PHOTO)) > 0,
                cursor.getString(cursor.getColumnIndex(POST_COLUMN_AUTHOR)),
                cursor.getInt(cursor.getColumnIndex(POST_COLUMN_AUTHOR_ID)),
                cursor.getString(cursor.getColumnIndex(POST_COLUMN_ADDED)),
                cursor.getInt(cursor.getColumnIndex(POST_COLUMN_GROUP)));
                */
        return null;
    }

    public Cursor getPostsCursor(int groupId, @Nullable String searchText){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + POST_TABLE_NAME + " WHERE " + POST_COLUMN_GROUP + " = " + groupId;
        if(searchText != null){
            query += " AND " + POST_COLUMN_BODY + " LIKE '%" + searchText + "%'";
        }
        return db.rawQuery(query, null);
    }

    public Bitmap addImage(int imageId, Bitmap image){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(IMAGE_COLUMN_ID, imageId);
        v.put(IMAGE_COLUMN_IMAGE, getBytesFromBitmap(image));
        db.insert(IMAGE_TABLE_NAME, null, v);
        Log.d(TAG, "Image: " + imageId + " created");
        return image;
    }

    public Bitmap getImageSmall(int imageId){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT " + IMAGE_COLUMN_IMAGE + " FROM " + IMAGE_TABLE_NAME +" WHERE " + IMAGE_COLUMN_ID + "=?";
        Cursor c = db.rawQuery(query, new String[] { String.valueOf(imageId) });
        Bitmap image = null;
        if (c.moveToFirst()) {
            byte[] data = c.getBlob(c.getColumnIndex(IMAGE_COLUMN_IMAGE));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            image = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            int height = (image.getHeight() * 300 / image.getWidth());
            image = getResizedBitmap(image, 300, height);

            Log.d(TAG, "image size " + image.getHeight() + ", " +  image.getWidth() + ", " + image.getByteCount());
        }
        c.close();

        return image;
    }

    public Bitmap getImageLarge(int imageId){
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT " + IMAGE_COLUMN_IMAGE + " FROM " + IMAGE_TABLE_NAME +" WHERE " + IMAGE_COLUMN_ID + "=?";
        Cursor c = db.rawQuery(query, new String[] { String.valueOf(imageId) });
        Bitmap image = null;
        if (c.moveToFirst()) {
            byte[] data = c.getBlob(c.getColumnIndex(IMAGE_COLUMN_IMAGE));
            image = BitmapFactory.decodeByteArray(data, 0, data.length);
            Log.d(TAG, "image size " + image.getByteCount());
        }
        c.close();
        return image;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public void emptyDatabase(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + GROUP_TABLE_NAME);
        db.execSQL("DELETE FROM " + MESSAGE_TABLE_NAME);
    }
}