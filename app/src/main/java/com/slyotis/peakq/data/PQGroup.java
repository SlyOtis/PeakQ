package com.slyotis.peakq.data;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by slyotis on 26.09.16.
 */

public class PQGroup implements Serializable{
    public int id;
    public String name;
    public String about;
    public boolean isAdmin;
    public boolean isPrivate;
    public boolean isMember;
    public boolean getPush;
    public DateTime created;

    public PQGroup(int id, String name, String about, boolean isPrivate) {
        this.id = id;
        this.name = name;
        this.about = about;
        this.isPrivate = isPrivate;
        this.isMember = false;
        this.getPush = false;
    }

    public PQGroup(int id, String name, String about, boolean isPrivate,
                   boolean isMember, boolean isAdmin, boolean getPush, DateTime created) {
        this.id = id;
        this.name = name;
        this.about = about;
        this.isPrivate = isPrivate;
        this.isMember = isMember;
        this.getPush = getPush;
        this.isAdmin = isAdmin;
        this.created = created;
    }
}
