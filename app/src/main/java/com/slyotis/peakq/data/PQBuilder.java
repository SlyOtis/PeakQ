package com.slyotis.peakq.data;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by slyotis on 20.11.16.
 */

public class PQBuilder {

    public static final DateTimeFormatter defaultFormat =
            DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    public static PQMessage message(JSONObject obj) throws JSONException{
        PQOffer offer = null;
        String body = null;
        if(!obj.isNull("Offer")) offer = offer(obj.getJSONObject("Offer"));
        if(!obj.isNull("Body")) body = obj.getString("Body");
        return new PQMessage(obj.getInt("ID"),
                obj.getInt("SenderID"),
                obj.getString("SenderName"),
                obj.getInt("ThreadID"),
                body,
                obj.getInt("PhotoID"),
                DateTime.parse(obj.getString("Sendt"), defaultFormat),
                offer);
    }

    public static PQGroup group(JSONObject obj) throws JSONException{
        return new PQGroup(obj.getInt("ID"),
                obj.getString("Name"),
                obj.getString("About"),
                obj.getBoolean("Private"),
                obj.getBoolean("Member"),
                obj.getBoolean("Admin"),
                obj.getBoolean("Push"),
                DateTime.parse(obj.getString("Added"), defaultFormat));
    }

    public static PQOffer offer(JSONObject obj) throws JSONException{
        return new PQOffer(obj.getInt("ID"),
                obj.getInt("Amount"),
                obj.getInt("SenderID"),
                obj.getInt("PostID"),
                obj.getInt("ThreadID"),
                obj.getInt("Type"),
                obj.getString("SenderName"),
                obj.getString("PostTitle"),
                DateTime.parse(obj.getString("Created"), defaultFormat),
                obj.getInt("Status"),
                DateTime.parse(obj.getString("Expires"), defaultFormat));
    }

    public static PQThread thread(JSONObject obj) throws  JSONException{
        return new PQThread(obj.getInt("ID"),
                obj.getString("Title"),
                obj.getString("PostTitle"),
                obj.getBoolean("Private"),
                obj.getBoolean("Push"),
                obj.getInt("PostID"),
                obj.getBoolean("Admin"),
                DateTime.parse(obj.getString("Started"), defaultFormat),
                DateTime.parse(obj.getString("Expires"), defaultFormat));
    }

    public static PQPost post(JSONObject obj) throws  JSONException{
        return new PQPost(obj.getInt("ID"),
                obj.getString("Title"),
                obj.getString("Description"),
                obj.getInt("AuthorID"),
                obj.getString("AuthorName"),
                DateTime.parse(obj.getString("Added"), defaultFormat),
                obj.getInt("GroupID"),
                obj.getBoolean("Solved"),
                obj.getInt("PhotoID"),
                DateTime.parse(obj.getString("Expires"), defaultFormat));
    }
}
