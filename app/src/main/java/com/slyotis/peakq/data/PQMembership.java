package com.slyotis.peakq.data;

import java.io.Serializable;

/**
 * Created by slyotis on 23.11.16.
 */

public class PQMembership implements Serializable {

    public boolean getPush;
    public boolean isMember;
    public boolean isAdmin;
    public int refId;

    public PQMembership(int refId, boolean getPush, boolean isMember, boolean isAdmin) {
        this.getPush = getPush;
        this.isMember = isMember;
        this.isAdmin = isAdmin;
        this.refId = refId;
    }
}
