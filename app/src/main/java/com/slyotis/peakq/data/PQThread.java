package com.slyotis.peakq.data;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by slyotis on 09.11.16.
 */

public class PQThread implements Serializable{
    public int id;
    public String title;
    public String postTitle;
    public boolean isPrivate;
    public boolean push;
    public int postId;
    public boolean isAdmin;
    public DateTime created;
    public DateTime expires;

    public PQThread(int id, String title, String postTitle, boolean isPrivate, boolean push,
                    int postId, boolean isAdmin, DateTime created, DateTime expires) {
        this.id = id;
        this.title = title;
        this.postTitle = postTitle;
        this.isPrivate = isPrivate;
        this.push = push;
        this.postId = postId;
        this.isAdmin = isAdmin;
        this.created = created;
        this.expires = expires;
    }
}
