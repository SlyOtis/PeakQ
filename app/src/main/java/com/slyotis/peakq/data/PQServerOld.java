package com.slyotis.peakq.data;


import android.app.Activity;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by slyotis on 23.09.16.
 */
@Deprecated
public class PQServerOld {

    private static final String TAG = PQServerOld.class.getName();
    private static final String SOURCE = "http://peakq.no/peakq/dev.php";

    public static final int ERROR_DUPLICATE_EMAIL = 0;
    public static final int ERROR_DUPLICATE_PHONE = 1;
    public static final int ERROR_BAD_CONNECTION = 3;
    public static final int ERROR_UNEXPECTED = 4;
    public static final int ERROR_NO_MATCH = 5;
    public static final int ERROR_DUPLICATE_NAME = 6;
    public static final int ERROR_NO_IMAGE = 7;

    public interface Callback{
        void onFailure(int errorCode);
        void onSuccess(int requestId, final String response);
    }

    public interface ImageCallback{
        void onFailure(int errorCode);
        void onSuccess(int requestId, final InputStream response);
    }

    private static void post(FormBody body, okhttp3.Callback callback){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        final Request request =  new Request.Builder().url(urlBuilder.build())
                .post(body).build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);

    }

    private static void get(okhttp3.Callback callback, String[] ... parms){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        for(String[] param: parms){
            urlBuilder.addQueryParameter(param[0], param[1]);
        }
        final Request request = new Request.Builder().url(urlBuilder.build())
                .get().build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);
    }

    private static abstract class ServerCallback implements okhttp3.Callback{

        private Callback callback;
        private Activity activity;
        private int requestId;

        public ServerCallback(int requestId, Callback callback, Activity activity) {
            this.callback = callback;
            this.activity = activity;
            this.requestId = requestId;
        }

        @Override
        public void onFailure(Call call, IOException e) {
            Log.d(TAG, e.getMessage());
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.onFailure(ERROR_BAD_CONNECTION);
                }
            });
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if(!response.isSuccessful()){
                Log.d(TAG, response.body().string());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(ERROR_BAD_CONNECTION);
                    }
                });
            } else {
                final String r = response.body().string();
                Log.d(TAG, "Testing response: " + r);
                if(checkResponse(r)){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSuccess(requestId, r);
                        }
                    });
                    return;
                } else Log.d(TAG, r);
            }
        }
        // Must override
        public abstract boolean checkResponse(String response);
    }

    private static class ServerImageCallback implements okhttp3.Callback{

        private ImageCallback callback;
        private Activity activity;
        private int requestId;

        public ServerImageCallback(int requestId, ImageCallback callback, Activity activity) {
            this.callback = callback;
            this.activity = activity;
            this.requestId = requestId;
        }

        @Override
        public void onFailure(Call call, IOException e) {
            Log.d(TAG, e.getMessage());
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.onFailure(ERROR_BAD_CONNECTION);
                }
            });
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            if(!response.isSuccessful()){
                Log.d(TAG, response.body().string());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(ERROR_BAD_CONNECTION);
                    }
                });
            } else {
                final InputStream r = response.body().byteStream();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(requestId, r);
                    }
                });
            }
        }
    }

    public static void newUser(PQUser user, final Callback callback, final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "newuser");
        builder.add("firstName", user.firstName);
        builder.add("lastName", user.lastName);
        builder.add("phone", user.phone);
        builder.add("email", user.email);
        builder.add("password", user.password);

        post(builder.build(), new ServerCallback(user.id, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!DuplEmail")){
                    callback.onFailure(ERROR_DUPLICATE_EMAIL);
                    return false;
                } else if(response.contains("!DuplPhone")){
                    callback.onFailure(ERROR_DUPLICATE_PHONE);
                    return false;
                }
                return true;
            }
        });
    }

    public static void login(String username, String password, final Callback callback,
                             final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "login");
        builder.add("username", username);
        builder.add("password", password);

        post(builder.build(), new ServerCallback(-1, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Wrong")){
                    callback.onFailure(ERROR_NO_MATCH);
                    return false;
                }
                else return true;
            }
        });
    }

    //TODO: Must figure out
    public static void newMessage(){

    }

    public static void newGroup(int userid, String password, String name, String about,
                                final Callback callback, final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "newgroup");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);
        builder.add("name", name);
        builder.add("about", about);

        post(builder.build(), new ServerCallback(userid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Wrong")){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(ERROR_UNEXPECTED);
                        }
                    });
                    return false;
                }
                if(response.contains("!DuplName")){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(ERROR_DUPLICATE_NAME);
                        }
                    });
                    return false;
                }
                else return true;
            }
        });
    }

    public static void updateMemberships(int userid, String password, final Callback callback,
                                         final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();
        builder.add("cmd", "memberships");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);

        post(builder.build(), new ServerCallback(userid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Error")){
                    callback.onFailure(ERROR_UNEXPECTED);
                    return false;
                }
                else return true;
            }
        });
    }

    public static void updateMemberships(int userid, String password, int groupid
            , boolean member, boolean push, final Callback callback, final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "memberships");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);
        builder.add("groupid", String.valueOf(groupid));
        builder.add("change", member? "1" : "-1");
        builder.add("push", push? "true" : "");

        post(builder.build(), new ServerCallback(groupid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Error")){
                    callback.onFailure(ERROR_UNEXPECTED);
                    return false;
                }
                else return true;
            }
        });
    }

    public static void listGroups(int userid, String password, final Callback callback,
                                  final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "listgroups");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);

        post(builder.build(), new ServerCallback(userid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Error")){
                    callback.onFailure(ERROR_UNEXPECTED);
                    return false;
                }
                else return true;
            }
        });
    }

    public static void listMessages(int userid, String password, final Callback callback,
                                    final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "listmessages");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);

        post(builder.build(), new ServerCallback(userid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Error")){
                    callback.onFailure(ERROR_UNEXPECTED);
                    return false;
                }
                else return true;
            }
        });
    }

    public static void deleteMessage(int userid, String password, int messageid,
                                     final Callback callback, final Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "deletemessage");
        builder.add("userid", String.valueOf(userid));
        builder.add("password", password);
        builder.add("messageid", String.valueOf(messageid));

        post(builder.build(), new ServerCallback(messageid, callback, activity){
            @Override
            public boolean checkResponse(String response) {
                if(response.contains("!Error")){
                    callback.onFailure(ERROR_UNEXPECTED);
                    return false;
                }
                else return true;
            }
        });
    }

    public static void getImage(int imageId, final ImageCallback callback, final Activity activity){
        get(new ServerImageCallback(imageId, callback, activity), new String[]{"img", String.valueOf(imageId)});
    }

    public static Response getImage(int imageId){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        urlBuilder.addQueryParameter("img", imageId + "");
        final Request request = new Request.Builder().url(urlBuilder.build())
                .get().build();

        OkHttpClient client = new OkHttpClient();
        try{
            return client.newCall(request).execute();
        }catch (IOException ex){
            Log.d(TAG, ex.getMessage());
        }
        return null;
    }
}
