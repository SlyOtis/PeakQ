package com.slyotis.peakq.data;

import java.io.Serializable;

/**
 * Created by slyotis on 26.09.16.
 */

public class PQUser implements Serializable{
    public String firstName;
    public String lastName;
    public String phone;
    public String email;
    public String password;
    public int credits;
    public int id;

    public PQUser(String firstName, String lastName, String phone,
                  String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.credits = 0;
    }

    public PQUser(int id, String firstName, String lastName, String email, String phone,
                  int credits, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.credits = credits;
        this.id = id;
    }
}
