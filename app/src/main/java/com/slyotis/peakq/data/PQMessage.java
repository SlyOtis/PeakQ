package com.slyotis.peakq.data;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by slyotis on 08.11.16.
 */

public class PQMessage implements Serializable {

    public int id;
    public int senderId;
    public String senderName;
    public int threadId;
    public boolean hasBody;
    public String body;
    public int photoId;
    public PQOffer offer;
    public boolean hasOffer;
    public boolean hasImage;
    public DateTime added;


    public PQMessage(int id, int senderId, String senderName, int threadId, String body,
                     int photoId, DateTime added, PQOffer offer) {
        this.id = id;
        this.senderId = senderId;
        this.senderName = senderName;
        this.threadId = threadId;
        this.body = body;
        this.photoId = photoId;
        this.hasBody = body == null;
        this.hasImage = photoId != 0;
        this.added = added;
        this.hasOffer = offer != null;
        this.offer = offer;
    }
}
