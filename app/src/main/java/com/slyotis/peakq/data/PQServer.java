package com.slyotis.peakq.data;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Vector;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by slyotis on 23.09.16.
 */

public class PQServer {

    private static final String TAG = PQServer.class.getName();
    private static final String SOURCE = "http://peakq.no/peakq/server.php";

    public static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    public static final int ERROR_CONNECTION = 0;
    public static final int ERROR_BAD_DATA = 1;

    private static PQUser currUser;

    public interface Callback<T>{
        void onFailure(int errorCode, String message);
        void onSuccess(int requestId, T response);
    }

    public interface ImageCallback{
        void onFailure(int errorCode, String message);
        void onSuccess(int requestId, final InputStream response);
    }

    private static void post(FormBody body, okhttp3.Callback callback){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        final Request request =  new Request.Builder().url(urlBuilder.build())
                .post(body).build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);

    }

    private static PQUser currUser(Activity activity){
        if(currUser == null){
            currUser = PQDataHandler.getCurrentUser(activity);
        }
        return currUser;
    }

    private static void post(FormBody body, byte[] imageData, okhttp3.Callback callback){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        Request request;
        if(imageData != null){
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.addFormDataPart("image", "image.png", RequestBody.create(MEDIA_TYPE_PNG, imageData));
            request =  new Request.Builder().url(urlBuilder.build())
                    .post(body)
                    .post(builder.build())
                    .build();
        } else {
            request =  new Request.Builder().url(urlBuilder.build())
                    .post(body).build();
        }

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);

    }

    private static void get(okhttp3.Callback callback, String[] ... parms){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        for(String[] param: parms){
            urlBuilder.addQueryParameter(param[0], param[1]);
        }
        final Request request = new Request.Builder().url(urlBuilder.build())
                .get().build();

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(callback);
    }

    private static abstract class ServerCallback<T> implements okhttp3.Callback{

        private Callback callback;
        private Activity activity;
        private int requestId;

        public ServerCallback(int requestId, Callback<T> callback, Activity activity) {
            this.callback = callback;
            this.activity = activity;
            this.requestId = requestId;
        }

        @Override
        public void onFailure(Call call, final IOException e) {
            Log.d(TAG, e.getMessage());
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.onFailure(ERROR_CONNECTION, e.getMessage());
                }
            });
        }

        @Override
        public void onResponse(Call call, final Response response) throws IOException {
            final String r = response.body().string();
            if(!response.isSuccessful()){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(ERROR_CONNECTION, response.message());
                    }
                });
            } else {
                try {
                    Log.d(TAG, r);
                    final JSONObject tmp = new JSONObject(r);
                    if (tmp.getString("status").equals("error")) {
                        final int errorCode = tmp.getInt("error_code");
                        final String message = tmp.getString("message");
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                callback.onFailure(errorCode, message);
                            }
                        });
                    } else {
                        final T result = buildResponse(tmp);
                        if(result == null){
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onFailure(ERROR_BAD_DATA, r);
                                }
                            });
                        } else {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onSuccess(requestId, result);
                                }
                            });
                        }
                    }
                } catch (JSONException ex){
                    Log.d(TAG, ex.getMessage());
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(ERROR_CONNECTION, response.message());
                        }
                    });
                }
            }
        }
        // Must override
        public abstract T buildResponse(JSONObject response);
    }

    private static class ServerImageCallback implements okhttp3.Callback{

        private ImageCallback callback;
        private Activity activity;
        private int requestId;

        public ServerImageCallback(int requestId, ImageCallback callback, Activity activity) {
            this.callback = callback;
            this.activity = activity;
            this.requestId = requestId;
        }

        @Override
        public void onFailure(Call call, final IOException e) {
            Log.d(TAG, e.getMessage());
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    callback.onFailure(ERROR_CONNECTION, e.getMessage());
                }
            });
        }

        @Override
        public void onResponse(Call call, final Response response) throws IOException {
            if(!response.isSuccessful()){
                Log.d(TAG, response.body().string());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onFailure(ERROR_CONNECTION, response.message());
                    }
                });
            } else {
                final InputStream r = response.body().byteStream();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onSuccess(requestId, r);
                    }
                });
            }
        }
    }

    public static void login(final String username, final String password, final Callback<PQUser> callback,
                             final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "login");
                builder.add("name", username);
                builder.add("pass", password);

                post(builder.build(), new ServerCallback<PQUser>(-1, callback, activity){
                    @Override
                    public PQUser buildResponse(JSONObject response) {
                        try {
                            JSONObject obj = response.getJSONObject("user");
                            PQUser user = new PQUser(obj.getInt("ID"),
                                    obj.getString("FirstName"),
                                    obj.getString("LastName"),
                                    obj.getString("Email"),
                                    obj.getString("Phone"),
                                    obj.getInt("Credit"),
                                    password);
                            return user;
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void logout(final String deviceName,
                              final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();


                builder.add("cmd", "logout");
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pass", currUser(activity).password);
                builder.add("dname", deviceName);

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), null, 0);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void createUser(final String email, final String password, final String phone,
                                  final String firstName, final String lastName,
                                  final Callback<PQUser> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Creating a new user");
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_user");
                builder.add("email", email);
                builder.add("pass", password);
                builder.add("phone", phone);
                builder.add("fname", firstName);
                builder.add("lname", lastName);

                post(builder.build(), new ServerCallback<PQUser>(-1, callback, activity){
                    @Override
                    public PQUser buildResponse(JSONObject response) {
                        try {
                            return new PQUser(response.getInt("userId"), firstName, lastName,
                                    email, phone, 0, password);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void createToken(final String name,
                                  final String token, final Callback<PQInfo> callback,
                                   final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_token");
                builder.add("name", name);
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("token", token);
                builder.add("type", "0");

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    "New token", currUser(activity).id);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteToken(final int tokenId,
                                   final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_token");
                builder.add("toid", String.valueOf(tokenId));
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    "Delete token", tokenId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updateUser(final String newEmail,
                                  final String newPhone, final String newPassword,
                                  final String firstName, final String lastName,
                                  final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_user");
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pass", currUser(activity).password);
                if(newPhone != null)builder.add("phone", newPhone);
                if(firstName != null)builder.add("fname", firstName);
                if(lastName != null) builder.add("lname", lastName);
                if(newEmail != null) builder.add("email", newEmail);
                if(newPassword != null)builder.add("npass", newPassword);

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    "Update user", currUser(activity).id);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteUser(final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_user");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    "Delete user", currUser(activity).id);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void createGroup(final String name,
                                   final String about, final boolean isPrivate, final boolean push,
                                   final Callback<PQGroup> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("name", name);
                builder.add("about", about);
                builder.add("private", isPrivate ? "1" : "0");
                builder.add("push", push ? "1" : "0");

                post(builder.build(), new ServerCallback<PQGroup>(-1, callback, activity){
                    @Override
                    public PQGroup buildResponse(JSONObject response) {
                        try {
                            return new PQGroup(response.getInt("groupId"), name, about, isPrivate,
                                    true, true, push, DateTime.now());
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();

    }

    public static void updateGroup(final int groupId,
                                   final String newName, final String newAbout,
                                   final boolean isPrivate, final Callback<PQInfo> callback,
                                   final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));
                if(newName != null) builder.add("name", newName);
                if(newAbout != null) builder.add("about", newAbout);
                builder.add("private", isPrivate ? "1" : "0");

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Update post", groupId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updateGroup(final int groupId,
                                   final String newName, final String newAbout,
                                   final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));
                if(newName != null) builder.add("name", newName);
                if(newAbout != null) builder.add("about", newAbout);

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Update post", groupId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getGroups(final int count,
                                 final int start, final boolean isMember, final String searchName,
                                 final String searchAbout, final Callback<PQGroup[]> callback,
                                 final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_groups");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("count", String.valueOf(count));
                builder.add("start", String.valueOf(start));
                builder.add("member", String.valueOf(isMember));
                if(searchName != null) builder.add("sname", searchName);
                if(searchAbout != null) builder.add("sabout", searchAbout);

                post(builder.build(), new ServerCallback<PQGroup[]>(-1, callback, activity){
                    @Override
                    public PQGroup[] buildResponse(JSONObject response) {
                        try {
                            Vector<PQGroup> groups = new Vector<>();
                            JSONArray data = response.getJSONArray("groups");
                            for(int i = 0; i < data.length(); i++){
                                groups.add(PQBuilder.group(data.getJSONObject(i)));
                            }
                            return groups.toArray(new PQGroup[groups.size()]);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void leaveGroup(final int groupId,
                                  final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "leave_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Left group", groupId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void joinGroup(final int groupId,
                                   final boolean push, final Callback<PQInfo> callback,
                                 final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "join_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));
                builder.add("push", push ? "1" : "0");

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Joined group", groupId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteGroup(final int groupId,
                                    final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_group");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Delete post", groupId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updateMembership(final int membershipId,
                                        final boolean push, final Callback<PQMembership> callback,
                                        final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_membership");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("mid", String.valueOf(membershipId));
                builder.add("push", push ? "1" : "0");

                post(builder.build(), new ServerCallback<PQMembership>(-1, callback, activity){
                    @Override
                    public PQMembership buildResponse(JSONObject response) {
                        try {
                            return new PQMembership(membershipId, response.getBoolean("push"), false, false);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteMembership(final int membershipId,
                                        final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_membership");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("mid", String.valueOf(membershipId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Delete Membership",
                                    membershipId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }
    // TODO :: Update this for all functions using it
    public static void newOffer(final int threadId, final int amount, final int type, final String expires, final Callback<PQOffer> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_offer");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("amount", String.valueOf(amount));
                builder.add("pid", String.valueOf(threadId));
                builder.add("t",String.valueOf(type));
                builder.add("exp", expires);

                post(builder.build(), new ServerCallback<PQOffer>(-1, callback, activity){
                    @Override
                    public PQOffer buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.offer(response.getJSONObject("offer"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void sendCredit(final int offerId,
                                final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "send_credit");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Send credit", offerId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void acceptOffer(final int offerId,
                                    final Callback<PQOffer> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "accept_offer");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQOffer>(-1, callback, activity){
                    @Override
                    public PQOffer buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.offer(response.getJSONObject("offer"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void declineOffer(final int offerId,
                                   final Callback<PQOffer> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "decline_offer");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQOffer>(-1, callback, activity){
                    @Override
                    public PQOffer buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.offer(response.getJSONObject("offer"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void withdrawOffer(final int offerId, final Callback<PQOffer> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "withdraw_offer");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQOffer>(-1, callback, activity){
                    @Override
                    public PQOffer buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.offer(response.getJSONObject("offer"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void acceptCredit(final int offerId,
                                  final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "accept_credit");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Accept credit", offerId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getOffers(final int count,
                                 final int start, final Callback<PQOffer[]> callback,
                                 final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_offers");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("count", String.valueOf(count));
                builder.add("start", String.valueOf(start));

                post(builder.build(), new ServerCallback<PQOffer[]>(-1, callback, activity){
                    @Override
                    public PQOffer[] buildResponse(JSONObject response) {
                        try {
                            Vector<PQOffer> offers = new Vector<>();
                            JSONArray data = response.getJSONArray("offers");
                            for(int i = 0; i < data.length(); i++){
                                offers.add(PQBuilder.offer(data.getJSONObject(i)));
                            }
                            return  offers.toArray(new PQOffer[offers.size()]);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteOffer(final int offerId,
                                    final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_offer");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("oid", String.valueOf(offerId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Delete offer", offerId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void newMessage(final int threadId,
                                  final byte[] imageData, final String messageContent, final int offerId,
                                   final Callback<PQMessage> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_message");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("tid", String.valueOf(threadId));
                builder.add("oid", String.valueOf(offerId));
                if(messageContent != null ) builder.add("body", messageContent);

                post(builder.build(), imageData, new ServerCallback<PQMessage>(-1, callback, activity){
                    @Override
                    public PQMessage buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.message(response.getJSONObject("message"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void newMessage(final int threadId,
                                  final byte[] imageData, final String messageContent,
                                  final Callback<PQMessage> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_message");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("tid", String.valueOf(threadId));
                if(messageContent != null ) builder.add("body", messageContent);

                post(builder.build(), imageData, new ServerCallback<PQMessage>(-1, callback, activity){
                    @Override
                    public PQMessage buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.message(response.getJSONObject("message"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getMessages(final int count,
                                    final int start, final int threadId,
                                   final Callback<PQMessage[]> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_messages");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("count", String.valueOf(count));
                builder.add("start", String.valueOf(start));
                builder.add("tid", String.valueOf(threadId));

                post(builder.build(), new ServerCallback<PQMessage[]>(-1, callback, activity){
                    @Override
                    public PQMessage[] buildResponse(JSONObject response) {
                        try {
                            Vector<PQMessage> messages = new Vector<>();
                            JSONArray data = response.getJSONArray("messages");
                            for(int i = 0; i < data.length(); i++){
                                messages.add(PQBuilder.message(data.getJSONObject(i)));
                            }
                            return  messages.toArray(new PQMessage[messages.size()]);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteMessage(final int messageId,
                                  final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_message");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("mid", String.valueOf(messageId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Delete message", messageId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void createPost(final int groupId,
                                  final String expires, final String title, final String description,
                                  final byte[] imageData, final Callback<PQPost> callback,
                                  final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();
                builder.add("cmd", "new_post");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));
                builder.add("exp", expires);
                builder.add("title", title);
                builder.add("desc", description);

                post(builder.build(), imageData, new ServerCallback<PQPost>(-1, callback, activity){
                    @Override
                    public PQPost buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.post(response.getJSONObject("post"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updatePost(final int postId,
                                  final String newExpires, final String newTitle, final String newDescription,
                                  final byte[] imageData, final Callback<PQInfo> callback,
                                  final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();
                builder.add("cmd", "update_post");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pid", String.valueOf(postId));
                if(newExpires != null) builder.add("exp", newExpires);
                if(newTitle != null) builder.add("title", newTitle);
                if(newDescription != null) builder.add("desc", newDescription);

                post(builder.build(), imageData, new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Update post", postId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getPosts(final int count, final int start,
                                final int groupId, final Callback<PQPost[]> callback,
                                 final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_posts");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("gid", String.valueOf(groupId));
                builder.add("count", String.valueOf(count));
                builder.add("start", String.valueOf(start));

                post(builder.build(), new ServerCallback<PQPost[]>(-1, callback, activity){
                    @Override
                    public PQPost[] buildResponse(JSONObject response) {
                        try {
                            Vector<PQPost> posts = new Vector<>();
                            JSONArray data = response.getJSONArray("posts");
                            for(int i = 0; i < data.length(); i++){
                                posts.add(PQBuilder.post(data.getJSONObject(i)));
                            }
                            return  posts.toArray(new PQPost[posts.size()]);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deletePost(final int postId, final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_post");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pid", String.valueOf(postId));


                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"), "Delete post", postId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void createThread(final int postId,
                                    final boolean push, final boolean isPrivate,
                                    final Callback<PQThread> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "new_thread");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pid", String.valueOf(postId));
                builder.add("push", push ? "1" : "0");
                builder.add("private", isPrivate ? "1" : "0");

                post(builder.build(), new ServerCallback<PQThread>(-1, callback, activity){
                    @Override
                    public PQThread buildResponse(JSONObject response) {
                        try {
                            return PQBuilder.thread(response.getJSONObject("thread"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updateThread(final int threadId,
                                    final String newTitle, final boolean isPrivate,
                                    final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_thread");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("tid", String.valueOf(threadId));
                if(newTitle != null) builder.add("title", newTitle);
                builder.add("private", isPrivate ? "1" : "0");

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    response.getString("thread_title"),
                                    response.getInt("threadId"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void updateThread(final int threadId,
                                    final String newTitle, final Callback<PQInfo> callback,
                                    final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "update_thread");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("tid", String.valueOf(threadId));
                if(newTitle != null) builder.add("title", newTitle);

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    response.getString("thread_title"),
                                    response.getInt("threadId"));
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getThreads(final int count,
                                final int start, final Callback<PQThread[]> callback,
                                final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_threads");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("count", String.valueOf(count));
                builder.add("start", String.valueOf(start));

                post(builder.build(), new ServerCallback<PQThread[]>(-1, callback, activity){
                    @Override
                    public PQThread[] buildResponse(JSONObject response) {
                        try {
                            Vector<PQThread> threads = new Vector<>();
                            JSONArray data = response.getJSONArray("threads");
                            for(int i = 0; i < data.length(); i++){
                                threads.add(PQBuilder.thread(data.getJSONObject(i)));
                            }
                            return threads.toArray(new PQThread[threads.size()]);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void deleteThread(final int threadId,
                                    final Callback<PQInfo> callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "delete_thread");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("tid", String.valueOf(threadId));

                post(builder.build(), new ServerCallback<PQInfo>(-1, callback, activity){
                    @Override
                    public PQInfo buildResponse(JSONObject response) {
                        try {
                            return new PQInfo(response.getString("status"),
                                    "Delete thread", threadId);
                        }catch (JSONException ex){
                            Log.d(TAG, ex.getMessage());
                            return null;
                        }
                    }
                });
            }
        }).start();
    }

    public static void getImage(final int imageId,
                                    final ImageCallback callback, final Activity activity){
        new Thread(new Runnable() {
            @Override
            public void run() {

                FormBody.Builder builder = new FormBody.Builder();

                builder.add("cmd", "get_image");
                builder.add("pass", currUser(activity).password);
                builder.add("id", String.valueOf(currUser(activity).id));
                builder.add("pid", String.valueOf(imageId));

                post(builder.build(), new ServerImageCallback(-1, callback, activity));

            }
        }).start();
    }

    public static byte[] getImage(final int imageId, Activity activity){
        FormBody.Builder builder = new FormBody.Builder();

        builder.add("cmd", "get_image");
        builder.add("pass", currUser(activity).password);
        builder.add("id", String.valueOf(currUser(activity).id));
        builder.add("pid", String.valueOf(imageId));

        HttpUrl.Builder urlBuilder = HttpUrl.parse(SOURCE).newBuilder();
        final Request request =  new Request.Builder().url(urlBuilder.build())
                .post(builder.build()).build();

        OkHttpClient client = new OkHttpClient();
        try{
            return client.newCall(request).execute().body().bytes();
        } catch (IOException ex){
            Log.d(TAG, ex.getMessage());
            return null;
        }
    }
}
