package com.slyotis.peakq.data;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by slyotis on 08.11.16.
 */

public class PQOffer implements Serializable{

    public static final int PENDING = 0;
    public static final int ACCEPTED = 1;
    public static final int DECLINED = 2;
    public static final int COMPLETE = 3;

    public static final int TYPE_OFFER = 0;
    public static final int TYPE_REQUEST = 1;

    public int id;
    public int amount;
    public int senderId;
    public int postId;
    public int threadId;
    public int status;
    public int type;
    public String senderName;
    public String postTitle;
    public DateTime created;
    public DateTime sendt;
    public DateTime recieved;
    public DateTime expires;

    public PQOffer(int id, int amount, int senderId, int postId, int threadId, int type,
                   String senderName, String postTitle, DateTime created, int status, DateTime expires) {
        this.id = id;
        this.amount = amount;
        this.senderId = senderId;
        this.postId = postId;
        this.senderName = senderName;
        this.postTitle = postTitle;
        this.created = created;
        this.status = status;
        this.type = type;
        this.expires = expires;
        this.threadId = threadId;
    }
}
