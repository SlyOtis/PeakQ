package com.slyotis.peakq.login_pager;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.slyotis.peakq.Login;
import com.slyotis.peakq.Main;
import com.slyotis.peakq.R;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.firebase.FBUser;
import com.slyotis.peakq.firebase.PQFire;

/**
 * A login screen that offers login via email/password.
 */
public class LoginLogin extends Fragment implements OnCompleteListener<AuthResult>{

    // For debugging purposes
    private static final String TAG = LoginLogin.class.getName();
    // UI references.
    private TextInputEditText mUsername;
    private TextInputEditText mPasswordView;
    private TextView welcome;
    private ImageView logo;
    // FireBase analytics
    private FirebaseAnalytics analytics;

    public static LoginLogin newInstance(){
        return new LoginLogin();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_login, container, false);
        analytics = FirebaseAnalytics.getInstance(getActivity());
        // Set up the login form.
        mUsername = (TextInputEditText) root.findViewById(R.id.login_username);
        mPasswordView = (TextInputEditText) root.findViewById(R.id.login_password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        welcome = (TextView)root.findViewById(R.id.login_welcome);
        logo = (ImageView) root.findViewById(R.id.login_logo);

        Button loginBtn = (Button) root.findViewById(R.id.login_action_login);
        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        final Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.pulsating);
        final View register = root.findViewById(R.id.login_action_register);
        register.setAnimation(anim);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.pager.setCurrentItem(1, true);
            }
        });

        return root;
    }

    private void attemptLogin() {

        // Reset errors.
        mUsername.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsername.getText().toString();
        String password = mPasswordView.getText().toString();

        // View to focus error, cancel = true if error
        TextInputEditText errorView = null;
        boolean cancel = false;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.login_error_invalid_password));
            errorView = mPasswordView;
            cancel = true;
            Log.d(TAG, "Invalid password");
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsername.setError(getString(R.string.login_error_field_required));
            errorView = mUsername;
            cancel = true;
            Log.d(TAG, "Missing username");
        } else if (!isUsernameValid(username)) {
            mUsername.setError(getString(R.string.login_error_invalid_email));
            errorView = mUsername;
            cancel = true;
            Log.d(TAG, "Invalid username");
        }

        // Brings focus to the error view if any errors
        if(cancel){
            errorView.requestFocus();
        } else {
            showProgress(true);

            FirebaseAuth auth = FirebaseAuth.getInstance();
            auth.signInWithEmailAndPassword(username, password)
                    .addOnCompleteListener(this);
        }
    }

    private boolean isUsernameValid(String username) {
        if(username.matches("^[0-9]+$")) return true;
        else return android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }

    private boolean isPasswordValid(String password) {
        //TODO: Need do discuss logic
        return password.length() > 4;
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if(task.isSuccessful()){
            Log.d(TAG, "Logged in: " + task.getResult().getUser().getEmail());
            showProgress(false);

            Intent intent = new Intent(getContext(), Main.class);
            startActivity(intent);
            getActivity().finish();

        } else {
            showProgress(false);
            // TODO: Add error message handling
            final View errorTmp = mPasswordView;
            mPasswordView.setError(getString(R.string.server_error_no_match));
            mPasswordView.post(new Runnable() {
                @Override
                public void run() {
                    errorTmp.requestFocus();
                }
            });
        }
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        welcome.setText(show ? R.string.login_loading: R.string.login_welcome);
        logo.setPivotX(logo.getWidth() / 2);
        logo.setPivotY(logo.getHeight() / 2);
        if(show) {
            RotateAnimation rotate = new RotateAnimation(0.f, 360.f,Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(shortAnimTime);
            rotate.setStartOffset(100);
            rotate.setInterpolator(new BounceInterpolator());
            rotate.setRepeatMode(Animation.RESTART);
            rotate.setRepeatCount(Animation.INFINITE);
            logo.startAnimation(rotate);
        } else {
            logo.clearAnimation();
            logo.animate()
                    .setDuration(shortAnimTime)
                    .setInterpolator(new BounceInterpolator())
                    .rotation(0.f)
                    .start();
        }
    }
}

