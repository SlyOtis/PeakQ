package com.slyotis.peakq.login_pager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.TextInputEditText;
import android.view.inputmethod.EditorInfo;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.slyotis.peakq.Login;
import com.slyotis.peakq.Main;
import com.slyotis.peakq.R;
import com.slyotis.peakq.firebase.FBUser;
import com.slyotis.peakq.firebase.PQFire;

/**
 * Created by slyotis on 17.09.16.
 */
public class LoginRegister extends Fragment implements OnCompleteListener<AuthResult>{

    private static final String TAG = LoginRegister.class.getName();

    private TextInputEditText firstName;
    private TextInputEditText lastName;
    private TextInputEditText email;
    private TextInputEditText phone;
    private TextInputEditText password;
    private TextInputEditText confirm;
    private ProgressBar progressBar;

    String _firstName;
    String _lastName;
    String _email;
    String _phone;
    String _password;
    String _confirm;

    public static LoginRegister newInstance(){
        return new LoginRegister();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.login_register, container, false);

        firstName = (TextInputEditText)root.findViewById(R.id.register_first_name);
        lastName = (TextInputEditText)root.findViewById(R.id.register_last_name);
        email = (TextInputEditText)root.findViewById(R.id.register_email);
        phone = (TextInputEditText)root.findViewById(R.id.register_phone);
        password = (TextInputEditText)root.findViewById(R.id.register_password);
        confirm = (TextInputEditText)root.findViewById(R.id.register_password_confirm);
        progressBar = (ProgressBar)root.findViewById(R.id.register_progress);

        confirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    registerUser();
                    return true;
                } else return false;
            }
        });

        firstName.requestFocus();

        (root.findViewById(R.id.cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.pager.setCurrentItem(0, true);
            }
        });
        root.findViewById(R.id.register_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        return root;
    }

    private void registerUser(){
        // Reset error
        firstName.setError(null);
        lastName.setError(null);
        email.setError(null);
        phone.setError(null);
        password.setError(null);
        confirm.setError(null);

        // Set input values
        _firstName = firstName.getText().toString();
        _lastName = lastName.getText().toString();
        _email = email.getText().toString();
        _phone = phone.getText().toString();
        _password = password.getText().toString();
        _confirm = confirm.getText().toString();

        // View to focus error, cancel = true if error
        TextInputEditText errorView = null;
        boolean cancel = false;

        // Check fo valid first name
        if(!TextUtils.isEmpty(_firstName) && !isNameValid(_firstName)){
            firstName.setError(getString(R.string.register_error_invalid_name));
            errorView = firstName;
            cancel = true;
            Log.d(TAG, "Invalid first name");
        }

        // Check fo valid last name
        if(!TextUtils.isEmpty(_lastName) && !isNameValid(_lastName)){
            lastName.setError(getString(R.string.register_error_invalid_name));
            errorView = lastName;
            cancel = true;
            Log.d(TAG, "Invalid last name");
        }

        // Check for a valid email
        if (!TextUtils.isEmpty(_email) && !isEmailValid(_email)) {
            email.setError(getString(R.string.register_error_invalid_email));
            errorView = email;
            cancel = true;
            Log.d(TAG, "Invalid email");
        }

        // Check for a valid email
        if (!TextUtils.isEmpty(_phone) && !isPhoneValid(_phone)) {
            phone.setError(getString(R.string.register_error_invalid_phone));
            errorView = phone;
            cancel = true;
            Log.d(TAG, "Invalid phone");
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(_password) && !isPasswordValid(_password)) {
            password.setError(getString(R.string.register_error_invalid_password));
            errorView = password;
            cancel = true;
            Log.d(TAG, "Invalid password");
        }

        // Check for password match
        if(!_password.equals(_confirm)){
            confirm.setError(getString(R.string.register_error_mismatch_password));
            errorView = confirm;
            cancel = true;
            Log.d(TAG, "Password did not match");
        }

        // Brings focus to the error view if any errors
        if(cancel){
            errorView.requestFocus();
        } else {
            showProgress(true);
            FirebaseAuth auth = FirebaseAuth.getInstance();
            auth.createUserWithEmailAndPassword(_email, _password)
                    .addOnCompleteListener(this);

        }
    }

    private boolean isPhoneValid(String phone){
        return Patterns.PHONE.matcher(phone).matches();
    }

    private boolean isNameValid(String name){
        return name.matches("^[a-zA-Z]+$");
    }

    private boolean isEmailValid(String username) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }

    private boolean isPasswordValid(String password) {
        //TODO: Need do discuss logic
        return password.length() >= 4;
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        if(task.isSuccessful()){
            showProgress(false);
            Log.d(TAG, "User registered: " + task.getResult().getUser().getEmail());
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference(PQFire.TABLE_USERS);

            FBUser user = new FBUser(
                    task.getResult().getUser().getUid(),
                    _firstName,
                    _lastName,
                    _email,
                    _phone,
                    0);
            ref = ref.child(task.getResult().getUser().getUid());
            ref.setValue(user);

            Log.d(TAG, "key: " + ref.getKey());

            Intent intent = new Intent(getContext(), Main.class);
            startActivity(intent);
            getActivity().finish();

        } else {
            showProgress(false);
            // TODO: Add error message handling
            Log.d(TAG, task.getException().getMessage());
        }
    }
}
