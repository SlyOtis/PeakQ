package com.slyotis.peakq.login_pager;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.slyotis.peakq.login_pager.LoginLogin;
import com.slyotis.peakq.R;
import com.slyotis.peakq.login_pager.LoginRegister;

import java.util.Locale;

/**
 * Created by slyotis on 17.09.16.
 */
public class LoginPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_OF_PAGES = 2;
    private Activity parent;

    public LoginPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.parent = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return LoginLogin.newInstance();
            case 1:
                return LoginRegister.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_OF_PAGES;
    }
}
