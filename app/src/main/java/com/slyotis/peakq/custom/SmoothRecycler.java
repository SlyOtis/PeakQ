package com.slyotis.peakq.custom;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by slyotis on 02.12.16.
 */

public class SmoothRecycler extends RecyclerView {
    public SmoothRecycler(Context context) {
        super(context);
    }

    public SmoothRecycler(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SmoothRecycler(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


}
