package com.slyotis.peakq.custom;
import android.database.ContentObserver;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;

import java.util.HashSet;
import java.util.Set;


public abstract class RecyclerViewCursorAdapter<T, VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    private SortedList<T> sortedList;
    private Set<T> previousCursorContent = new HashSet<>();

    public RecyclerViewCursorAdapter(@NonNull Class<T> klass, @Nullable Cursor cursor) {
        sortedList = new SortedList<>(klass, new SortedListAdapterCallback<T>(this){
            @Override
            public int compare(T o1, T o2) {
                return RecyclerViewCursorAdapter.this.compare(o1, o2);
            }

            @Override
            public boolean areContentsTheSame(T oldItem, T newItem) {
                return RecyclerViewCursorAdapter.this.areContentsTheSame(oldItem, newItem);
            }

            @Override
            public boolean areItemsTheSame(T item1, T item2) {
                return RecyclerViewCursorAdapter.this.areItemsTheSame(item1, item2);
            }
        });
        setCursor(cursor);
    }

    public int compare(T o1, T o2){
        return 0;
    }

    public boolean areContentsTheSame(T oldItem, T newItem) {
        return false;
    }

    public boolean areItemsTheSame(T item1, T item2) {
        return false;
    }

    public void setCursor(Cursor cursor) {
        previousCursorContent.clear();
        sortedList.clear();
        switchCursor(cursor);
    }

    public void switchCursor(Cursor cursor) {

        if (cursor == null) {
            return;
        }

        Set<T> currentCursorContent = new HashSet<>();

        sortedList.beginBatchedUpdates();

        cursor.moveToPosition(-1);

        while (cursor.moveToNext()) {

            T item = fromCursorRow(cursor);

            currentCursorContent.add(item);

            sortedList.add(item);

        }

        for (T item : previousCursorContent) {

            if (!currentCursorContent.contains(item)) {
                sortedList.remove(item);
            }

        }

        sortedList.endBatchedUpdates();

        previousCursorContent = currentCursorContent;

    }

    public abstract T fromCursorRow(Cursor cursor);

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    public T getItem(int position) {
        return sortedList.get(position);
    }
}