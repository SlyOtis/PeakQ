package com.slyotis.peakq.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 29.11.16.
 */

public class ChatBubble extends RelativeLayout {

    private static final int TAIL_LEFT = 0;
    private static final int TAIL_RIGHT = 1;
    private static final int DEFAULT_RADIUS = 60;
    private static final int DEFAULT_TAIL_LENGTH = 40;
    private static final int DEFAULT_ICON_WIDTH = 120;
    private static final int DEFAULT_ICON_HEIGHT = 120;

    private int tailDirection;
    private int bubbleRadius;
    private int tailLength;
    private int bubbleIconRes;
    private int iconWidth;
    private int iconHeight;

    private int paddingEnd;
    private int paddingStart;
    private int paddingTop;
    private int paddingBottom;

    private RectF iconRect;
    private Path clipPath;
    private RectF clipRect;
    private Paint paint;

    private Bitmap bubbleIcon;
    int deviceWidth;

    public ChatBubble(Context context) {
        super(context);
        init(null, context);
    }

    public ChatBubble(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, context);
    }

    public ChatBubble(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, context);
    }

    public ChatBubble(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, context);
    }

    private void init(AttributeSet attrs, Context context){
        if(attrs != null) {
            TypedArray style = context.obtainStyledAttributes(attrs, R.styleable.ChatBubble);
            tailDirection = style.getInt(R.styleable.ChatBubble_tailDirection, TAIL_RIGHT);
            tailLength = style.getDimensionPixelSize(R.styleable.ChatBubble_tailLength, DEFAULT_TAIL_LENGTH);
            bubbleRadius = style.getDimensionPixelSize(R.styleable.ChatBubble_bubbleRadius, DEFAULT_RADIUS);
            bubbleIconRes = style.getResourceId(R.styleable.ChatBubble_bubbleIcon, -1);
            iconWidth = style.getDimensionPixelSize(R.styleable.ChatBubble_bubbleIconWidth, DEFAULT_ICON_WIDTH);
            iconHeight = style.getDimensionPixelSize(R.styleable.ChatBubble_bubbleIconHeight, DEFAULT_ICON_HEIGHT);
            style.recycle();
        } else {
            tailDirection = TAIL_RIGHT;
            tailLength = DEFAULT_TAIL_LENGTH;
            bubbleRadius = DEFAULT_RADIUS;
            bubbleIconRes = -1;
        }

        if(bubbleIconRes != -1){
            BitmapFactory.Options op = new BitmapFactory.Options();
            op.outHeight = iconHeight;
            op.outWidth = iconWidth;
            bubbleIcon = BitmapFactory.decodeResource(getResources(), bubbleIconRes, op);
            invalidate();
        }

        setWillNotDraw(false);
        clipPath = new Path();
        clipRect = new RectF();
        paint = new Paint();

        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point deviceDisplay = new Point();
        display.getSize(deviceDisplay);
        deviceWidth = deviceDisplay.x;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setSize(w, h);
        //setPadding(paddingStart, paddingTop, paddingEnd, paddingBottom);
    }

    @Override
    public int getPaddingEnd() {
        return super.getPaddingEnd() + paddingEnd;
    }

    @Override
    public int getPaddingTop() {
        return super.getPaddingTop() + paddingTop;
    }

    @Override
    public int getPaddingBottom() {
        return super.getPaddingBottom() + paddingBottom;
    }

    @Override
    public int getPaddingLeft() {
        return super.getPaddingLeft() + paddingStart;
    }

    @Override
    public int getPaddingStart() {
        return super.getPaddingStart() + paddingStart;
    }

    @Override
    public int getPaddingRight() {
        return super.getPaddingRight() + paddingEnd;
    }

    private void setSize(int w, int h){
        clipPath.reset();
        switch (tailDirection){
            case TAIL_LEFT:{
                if(bubbleIconRes != -1){
                    float y = (float)(bubbleRadius * Math.sin(45));
                    iconRect = new RectF(w - iconWidth, 0, w, iconHeight);
                    clipRect.set(tailLength, iconRect.top + y, iconRect.right - y, h);
                    clipPath.moveTo(0, clipRect.top);
                    clipPath.lineTo(clipRect.left + bubbleRadius, clipRect.top);
                    clipPath.lineTo(clipRect.left, y + bubbleRadius);
                    clipPath.addRoundRect(clipRect, bubbleRadius, bubbleRadius, Path.Direction.CW);
                } else {
                    clipRect.set(tailLength, 0, w, h);
                    clipPath.addRoundRect(clipRect, bubbleRadius, bubbleRadius, Path.Direction.CW);
                    clipPath.lineTo(bubbleRadius * 2, 0);
                    clipPath.lineTo(tailLength, bubbleRadius);
                }
                break;
            }
            case TAIL_RIGHT:{
                if(bubbleIconRes != -1){
                    float y = (float)(bubbleRadius * Math.sin(45));
                    iconRect = new RectF(0, 0, iconWidth, iconHeight);
                    clipRect.set(iconRect.left + y, iconRect.top + y, w - tailLength, h);
                    clipPath.addRoundRect(clipRect, bubbleRadius, bubbleRadius, Path.Direction.CCW);
                    clipPath.moveTo(w, clipRect.top);
                    clipPath.lineTo(w-(tailLength + bubbleRadius *2), clipRect.top);
                    clipPath.lineTo(clipRect.right, bubbleRadius + y);
                } else {
                    clipRect.set(0, 0, w - tailLength, h);
                    clipPath.addRoundRect(clipRect, bubbleRadius, bubbleRadius, Path.Direction.CCW);
                    clipPath.moveTo(w, 0);
                    clipPath.lineTo(w - (tailLength + bubbleRadius * 2), 0);
                    clipPath.lineTo(w - tailLength, bubbleRadius);
                }
                break;
            }
        }
        clipPath.close();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        paddingStart = (int)clipRect.left;
        paddingEnd = r - (int)clipRect.left - (int)clipRect.right;
        paddingTop = t - (int)clipRect.top;
        paddingBottom = b - (int)clipRect.top - (int)clipRect.bottom;
        setPadding(paddingStart, paddingTop, paddingEnd, paddingBottom);
        super.onLayout(changed, l, t, r, b);
    }



    @Override
    public void draw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.draw(canvas);
        canvas.restoreToCount(save);
        if(bubbleIcon != null){
            canvas.drawBitmap(bubbleIcon, null, iconRect, paint);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.onDraw(canvas);
        canvas.restoreToCount(save);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(clipPath);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }
}
