package com.slyotis.peakq.custom;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by slyotis on 14.11.16.
 */

public abstract class RecyclerArrayAdapter<VH extends RecyclerView.ViewHolder,T> extends RecyclerView.Adapter<VH> {

    private List<T> items;
    private boolean updateIfExist = true;

    public RecyclerArrayAdapter() {
        this.items = new ArrayList<>();
    }

    public abstract void onBindData(VH holder, T data, int viewType, int position);

    @Override
    public void onBindViewHolder(VH holder, int position) {
        onBindData(holder, getItem(position), getItemViewType(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setUpdateItemIfExist(boolean updateIfExist){
        updateIfExist = updateIfExist;
    }

    public abstract boolean itemExist(T oldItem, T newItem);

    public void removeItem(int position){
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void removeItem(T item){
        int index = items.indexOf(item);
        items.remove(index);
        notifyItemRemoved(index);
    }

    public void appendItems(T[] newItems){
        if(items.size() > 0) {
            List<T> toCheck = items;
            for (T newItem : newItems) {
                boolean exist = false;
                for (T oldItem : toCheck) {
                    if (itemExist(oldItem, newItem) && updateIfExist) {
                        exist = true;
                        int i = items.indexOf(oldItem);
                        items.set(i, newItem);
                        notifyItemChanged(i);
                        toCheck.remove(oldItem);
                        break;
                    }
                }
                if (!exist) {
                    items.add(newItem);
                    notifyItemInserted(items.indexOf(newItem));
                }
            }
        } else {
            setItems(newItems);
        }
    }

    public void updateItem(int position, T item){
        items.set(position, item);
        notifyItemChanged(position);
    }

    public void addItem(T newItem){
        for(int i = 0; i < items.size(); i++){
            if(itemExist(items.get(i), newItem)){
                items.set(i, newItem);
                notifyItemChanged(i);
                return;
            }
        }
        items.add(newItem);
        notifyItemInserted(items.indexOf(newItem));
    }

    public void setItems(T[] newItems){
        items.clear();
        items.addAll(Arrays.asList(newItems));
        notifyDataSetChanged();
    }

    public T getItem(int position){
        return items.get(position);
    }

    public int getDataCount(){
        return items.size();
    }

}
