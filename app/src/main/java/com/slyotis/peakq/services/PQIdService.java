package com.slyotis.peakq.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;

/**
 * Created by slyotis on 01.11.16.
 */

public class PQIdService extends FirebaseInstanceIdService{

    private static final String TAG = PQIdService.class.getName();

    public static String deviceName = "Android";
    public static String deviceToken;

    public PQIdService() {
        super();
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        deviceToken = refreshedToken;
    }

}
