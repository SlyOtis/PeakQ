package com.slyotis.peakq.services;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.slyotis.peakq.data.PQBuilder;
import com.slyotis.peakq.data.PQMessage;
import com.slyotis.peakq.data.PQOffer;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQThread;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

public class PQNotificationsService extends FirebaseMessagingService {

    private static final String TAG = PQNotificationsService.class.getName();

    public static final String CATEGORY_MESSAGE = "category_message";
    public static final String ACTION_NEW_MESSAGE = "action_new_message";
    public static final String ACTION_DELETE_MESSAGE = "action_delete_message";
    public static final String MESSAGE_DATA = "message_data";

    public static final String CATEGORY_POST = "category_post";
    public static final String ACTION_NEW_POST = "action_new_post";
    public static final String ACTION_DELETE_POST = "action_delete_post";
    public static final String POST_DATA = "post_data";

    public static final String CATEGORY_THREAD = "category_thread";
    public static final String ACTION_NEW_THREAD = "action_new_thread";
    public static final String ACTION_DELETE_THREAD = "action_delete_thread";
    public static final String THREAD_DATA = "thread_data";

    public static final String CATEGORY_OFFER = "category_offer";
    public static final String ACTION_NEW_OFFER = "action_new_offer";
    public static final String ACTION_DELETE_OFFER = "action_delete_offer";
    public static final String OFFER_DATA = "offer_data";

    private static final int MESSAGE = 1;
    private static final int OFFER = 0;
    private static final int POST = 2;
    private static final int THREAD = 3;
    private static final int DELETE = 1;
    private static final int CREATE = 0;

    public PQNotificationsService() {
    }
    private void onDeletePost(int postId){
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_POST);
        intent.setAction(ACTION_DELETE_POST);
        intent.putExtra(POST_DATA, postId);
        sendBroadcast(intent);
    }
    private void onNewPost(JSONObject obj) throws JSONException{
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_POST);
        intent.setAction(ACTION_NEW_POST);
        intent.putExtra(POST_DATA, PQBuilder.post(obj));
        sendBroadcast(intent);
    }
    private void onDeleteOffer(int offerId){
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_OFFER);
        intent.setAction(ACTION_DELETE_OFFER);
        intent.putExtra(OFFER_DATA, offerId);
        sendBroadcast(intent);
    }
    private void onNewOffer(JSONObject obj) throws JSONException{
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_OFFER);
        intent.setAction(ACTION_NEW_OFFER);
        intent.putExtra(OFFER_DATA, PQBuilder.offer(obj));
        sendBroadcast(intent);
    }
    private void onNewThread(JSONObject obj) throws JSONException{
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_THREAD);
        intent.setAction(ACTION_NEW_THREAD);
        intent.putExtra(THREAD_DATA, PQBuilder.thread(obj));
        sendBroadcast(intent);
    }

    private void onNewMessage(JSONObject obj) throws JSONException{
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_MESSAGE);
        intent.setAction(ACTION_NEW_MESSAGE);
        intent.putExtra(MESSAGE_DATA, PQBuilder.message(obj));
        sendBroadcast(intent);
    }

    private void onMessageDeleted(int msgId){
        Intent intent = new Intent();
        intent.addCategory(CATEGORY_MESSAGE);
        intent.setAction(ACTION_DELETE_MESSAGE);
        intent.putExtra(MESSAGE_DATA, msgId);
        sendBroadcast(intent);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            JSONObject info = new JSONObject(remoteMessage.getData().get("Info"));
            switch (info.getInt("Type")){
                case MESSAGE:{
                    if(info.getInt("Action") == CREATE){
                        onNewMessage(new JSONObject(remoteMessage.getData().get("Data")));
                    } else {
                        onMessageDeleted(Integer.valueOf(remoteMessage.getData().get("Data")));
                    }
                    break;
                }
                case THREAD:{
                    if(info.getInt("Action") == CREATE){
                        onNewThread(new JSONObject(remoteMessage.getData().get("Data")));
                    }
                    break;
                }
                case OFFER:{
                    if(info.getInt("Action") == CREATE){
                        onNewOffer(new JSONObject(remoteMessage.getData().get("Data")));
                    } else {
                        onDeleteOffer(Integer.valueOf(remoteMessage.getData().get("Data")));
                    }
                    break;
                }
                case POST:{
                    if(info.getInt("Action") == CREATE){
                        onNewPost(new JSONObject(remoteMessage.getData().get("Data")));
                    } else {
                        onDeletePost(Integer.valueOf(remoteMessage.getData().get("Data")));
                    }
                    break;
                }
            }

        } catch (JSONException ex){
            Log.d(TAG, ex.getMessage());
        }
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        remoteMessage.getData().get("Info");

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // TODO : ADD user authentication on server

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
}
