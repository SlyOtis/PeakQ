package com.slyotis.peakq.chat_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class BaseMessage extends RecyclerView.ViewHolder {

    public long revealDuration = 300;
    public View bubble;
    public LinearLayout root;
    public TextView message;
    public TextView date;
    public TextView time;
    public View timeContainer;

    public BaseMessage(View itemView) {
        super(itemView);
        root = (LinearLayout) itemView.findViewById(R.id.chat_message_root);
        bubble = itemView.findViewById(R.id.chat_message);
        message = (TextView) itemView.findViewById(R.id.chat_message_message);
        date = (TextView) itemView.findViewById(R.id.chat_message_date);
        time = (TextView) itemView.findViewById(R.id.chat_message_time);
        timeContainer = itemView.findViewById(R.id.chat_message_time_container);
    }

    public void showSentDate(boolean show){
        if(show){
            date.animate()
                    .alpha(1f)
                    .setDuration(revealDuration)
                    .setInterpolator(new DecelerateInterpolator())
                    .withStartAction(new Runnable() {
                        @Override
                        public void run() {
                            date.setAlpha(0f);
                            date.setVisibility(View.VISIBLE);
                        }
                    })
                    .start();
        } else {
            date.animate()
                    .alpha(0f)
                    .setDuration(revealDuration)
                    .setInterpolator(new DecelerateInterpolator())
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            date.setVisibility(View.GONE);
                        }
                    })
                    .start();
        }
    }

    public void showSentTime(boolean show){
        if(show){
            timeContainer.setAlpha(0f);
            timeContainer.setVisibility(View.VISIBLE);
            timeContainer.animate()
                    .alpha(1f)
                    .setDuration(revealDuration)
                    .setInterpolator(new DecelerateInterpolator())
                    .start();
        } else {
            timeContainer.animate()
                    .alpha(0f)
                    .setDuration(revealDuration)
                    .setInterpolator(new DecelerateInterpolator())
                    .withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            timeContainer.setVisibility(View.GONE);
                        }
                    })
                    .start();
        }
    }
}
