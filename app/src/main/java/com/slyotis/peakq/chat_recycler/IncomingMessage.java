package com.slyotis.peakq.chat_recycler;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class IncomingMessage extends ChatMessage{

    public TextView sender;

    public IncomingMessage(View itemView) {
        super(itemView);
        sender = (TextView) itemView.findViewById(R.id.chat_message_sender);
    }
}
