package com.slyotis.peakq.chat_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class ChatMessage extends BaseMessage{

    public ImageView image;

    public ChatMessage(View itemView) {
        super(itemView);
        image = (ImageView) itemView.findViewById(R.id.chat_message_image);
    }
}
