package com.slyotis.peakq.chat_recycler;

import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class OutgoingOfferMessage extends OfferMessage{

    public View withdraw;
    public View delete;

    public OutgoingOfferMessage(View itemView) {
        super(itemView);
        withdraw = itemView.findViewById(R.id.chat_offer_message_action_withdraw);
        delete = itemView.findViewById(R.id.chat_action_delete);
    }
}
