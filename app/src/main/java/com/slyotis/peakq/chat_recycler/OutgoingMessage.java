package com.slyotis.peakq.chat_recycler;

import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.slyotis.peakq.R;


/**
 * Created by slyotis on 22.09.16.
 */

public class OutgoingMessage extends ChatMessage {

    public View delete;
    public View messageContainer;

    public OutgoingMessage(View itemView) {
        super(itemView);
        delete = itemView.findViewById(R.id.chat_action_delete);
        messageContainer = itemView.findViewById(R.id.chat_message_message_container);
    }
}
