package com.slyotis.peakq.chat_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class IncomingOfferMessage extends OfferMessage{
    public View accept;
    public View decline;
    public View actionContainer;
    public IncomingOfferMessage(View itemView) {
        super(itemView);
        accept = itemView.findViewById(R.id.chat_offer_message_action_accept);
        decline = itemView.findViewById(R.id.chat_offer_message_action_decline);
        actionContainer = itemView.findViewById(R.id.chat_offer_message_action_container);

    }
}
