package com.slyotis.peakq.chat_recycler;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerArrayAdapter;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQInfo;
import com.slyotis.peakq.data.PQMessage;
import com.slyotis.peakq.data.PQOffer;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;

/**
 * Created by slyotis on 14.11.16.
 */

public class ChatArrayAdapter extends RecyclerArrayAdapter<BaseMessage, PQMessage>{

    private final static String TAG = ChatArrayAdapter.class.getName();

    public static final int MESSAGE_TYPE_INCOMING = 0;
    public static final int MESSAGE_TYPE_OUTGOING = 1;
    public static final int MESSAGE_TYPE_INCOMING_OFFER = 2;
    public static final int MESSAGE_TYPE_OUTGOING_OFFER = 3;

    private static final int OFFER_PENDING = 0;
    private static final int OFFER_ACCEPTED = 1;
    private static final int OFFER_DECLINED = 2;
    private static final int OFFER_COMPLETE = 3;
    private static final int OFFER_WITHDRAWN = 4;

    private Activity activity;
    private RecyclerView recyclerView;
    private boolean senderNameVisible = false;
    private long revealDuration = 300;

    private int deleteIndex = -1, sentTimeIndex = -1, oldPos = -1;

    private boolean animateIn = true;
    int lastPos = 0;

    public ChatArrayAdapter(Activity activity, RecyclerView recyclerView) {
        this.activity = activity;
        this.recyclerView = recyclerView;
        recyclerView.setItemAnimator(null);
    }


    @Override
    public boolean itemExist(PQMessage oldItem, PQMessage newItem) {
        return oldItem.id == newItem.id;
    }

    @Override
    public BaseMessage onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case MESSAGE_TYPE_INCOMING:
                return new IncomingMessage(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_message_incoming, parent, false));
            case MESSAGE_TYPE_INCOMING_OFFER:
                return new IncomingOfferMessage(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_message_offer_incoming, parent, false));
            case MESSAGE_TYPE_OUTGOING:
                return new OutgoingMessage(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_message_outgoing, parent, false));
            case MESSAGE_TYPE_OUTGOING_OFFER:
                return new OutgoingOfferMessage(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_message_offer_outgoing, parent, false));
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        PQMessage msg = getItem(position);
        if(PQDataHandler.getCurrentUser(activity).id == msg.senderId){
            if(msg.hasOffer) return MESSAGE_TYPE_OUTGOING_OFFER;
            else return MESSAGE_TYPE_OUTGOING;
        } else {
            if(msg.hasOffer) return MESSAGE_TYPE_INCOMING_OFFER;
            else return MESSAGE_TYPE_INCOMING;
        }
    }

    public void showSenderName(boolean showSenderName){
        senderNameVisible = showSenderName;
        notifyDataSetChanged();
    }

    public void hideSentTime(){

    }

    public void hideDelete(){
        TransitionManager.beginDelayedTransition(recyclerView,
                TransitionInflater.from(activity).inflateTransition(R.transition.message_time));
        int oldPos = deleteIndex;
        deleteIndex = -1;
        notifyItemChanged(oldPos);
    }

    public boolean isDeleteVisible(){
        return deleteIndex != -1;
    }

    private void runEnterAnimation(View view, int position) {
        if(position > lastPos && position != oldPos && position != deleteIndex && sentTimeIndex != position) {
            view.setTranslationY(recyclerView.getHeight());
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        } /*else {
            view.setTranslationY(-view.getHeight()*2);
            view.animate()
                    .translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        } */
        lastPos = position;
    }

    private void bindOutgoingOffer(final OutgoingOfferMessage msg, final PQMessage data, final int position){

        final boolean isVisible = position == deleteIndex;
        msg.delete.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        msg.delete.setActivated(isVisible);

        msg.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PQServer.deleteMessage(data.id, deleteCallback, activity);
            }
        });
        msg.bubble.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TransitionManager.beginDelayedTransition(recyclerView,
                        TransitionInflater.from(activity).inflateTransition(R.transition.message_time));
                oldPos = deleteIndex;
                deleteIndex = isVisible ? -1 : position;
                notifyItemChanged(deleteIndex);
                if (oldPos != -1) notifyItemChanged(oldPos);
                return true;
            }
        });

        final PQOffer offer = data.offer;
        msg.offerTitle.setText("Du har tilbydt");
        msg.amount.setText(String.valueOf(offer.amount));
        switch (offer.status){
            case OFFER_PENDING:
                msg.withdraw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PQServer.withdrawOffer(data.id, withdrawOfferCallback, activity);
                    }
                });
                msg.withdraw.setVisibility(View.VISIBLE);
                msg.status.setVisibility(View.GONE);
                msg.bubble.setBackgroundResource(R.drawable.chat_message_outgoing_offer);
                int color = ContextCompat.getColor(activity, R.color.chat_offer_message_text);
                msg.offerTitle.setTextColor(color);
                msg.message.setTextColor(color);
                msg.status.setTextColor(color);
                break;
            case OFFER_ACCEPTED:
                    msg.withdraw.setPivotX(100);
                    msg.withdraw.animate()
                            .setDuration(revealDuration)
                            .alpha(0f)
                            .scaleX(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.withdraw.setVisibility(View.GONE);
                                }
                            }).start();
                    msg.status.setPivotX(100f);
                    msg.status.animate()
                            .setDuration(revealDuration)
                            .alpha(1f)
                            .scaleX(1f)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.status.setText("Tilbud akkseptert");
                                    msg.status.setVisibility(View.VISIBLE);
                                    msg.status.setAlpha(0);
                                    msg.status.setScaleX(0);
                                }
                            }).start();

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_outgoing_offer_accepted);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_accepted_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                    msg.amount.setTextColor(color);
                break;
            case OFFER_DECLINED:
                    msg.withdraw.setPivotX(100f);
                    msg.withdraw.animate()
                            .setDuration(revealDuration)
                            .alpha(0f)
                            .scaleX(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.withdraw.setVisibility(View.GONE);
                                }
                            }).start();
                    msg.status.setPivotX(0);
                    msg.status.animate()
                            .setDuration(revealDuration)
                            .alpha(1f)
                            .scaleX(1f)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.status.setText("Tilbud avslått");
                                    msg.status.setVisibility(View.VISIBLE);
                                    msg.status.setAlpha(0f);
                                    msg.status.setScaleX(0f);
                                }
                            }).start();

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_outgoing_offer_declined);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_declined_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                    msg.amount.setTextColor(color);
                break;
            case OFFER_COMPLETE:
                    if(msg.withdraw.getVisibility() == View.VISIBLE){
                        msg.withdraw.setPivotY(100f);
                        msg.withdraw.animate()
                                .setDuration(revealDuration)
                                .alpha(0f)
                                .scaleY(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.withdraw.setVisibility(View.GONE);
                                    }
                                }).start();
                    }
                    if(msg.withdraw.getVisibility() == View.GONE){
                        msg.status.setPivotY(0);
                        msg.status.animate()
                                .setDuration(revealDuration)
                                .alpha(1f)
                                .scaleY(1f)
                                .withStartAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.status.setVisibility(View.VISIBLE);
                                        msg.status.setAlpha(0);
                                        msg.status.setScaleY(0);
                                        msg.status.setText("Tilbud ferdig");
                                    }
                                }).start();
                    }

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_outgoing_offer_complete);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_complete_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                    msg.amount.setTextColor(color);

                break;
            case OFFER_WITHDRAWN: {
                    if (msg.withdraw.getVisibility() == View.VISIBLE) {
                        msg.withdraw.setPivotY(100f);
                        msg.withdraw.animate()
                                .setDuration(revealDuration)
                                .alpha(0f)
                                .scaleY(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.withdraw.setVisibility(View.GONE);
                                    }
                                }).start();
                    }
                    if (msg.status.getVisibility() == View.GONE) {
                        msg.status.setPivotY(0);
                        msg.status.animate()
                                .setDuration(revealDuration)
                                .alpha(1f)
                                .scaleY(1f)
                                .withStartAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.status.setVisibility(View.VISIBLE);
                                        msg.status.setAlpha(0);
                                        msg.status.setScaleY(0);
                                        msg.status.setText("Tilbud trekkt");
                                    }
                                }).start();
                    }

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_outgoing_offer_complete);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_complete_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                    msg.amount.setTextColor(color);
                break;
            }
        }
        bindMessage(msg, data, position);
    }
    private void bindIncomingOffer(final IncomingOfferMessage msg, final PQMessage data, int position){
        final PQOffer offer = data.offer;
        msg.offerTitle.setText(data.senderName + " tilbyr");
        msg.amount.setText(String.valueOf(offer.amount));
        switch (offer.status){
            case OFFER_PENDING:
                msg.accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PQServer.acceptOffer(offer.id, acceptOfferCallback, activity);
                    }
                });
                msg.decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PQServer.declineOffer(offer.id, declineOfferCallback, activity);
                    }
                });
                msg.actionContainer.setVisibility(View.VISIBLE);
                msg.status.setVisibility(View.GONE);
                msg.bubble.setBackgroundResource(R.color.chat_offer_message_incoming_background);
                int color = ContextCompat.getColor(activity, R.color.chat_offer_message_text);
                msg.offerTitle.setTextColor(color);
                msg.message.setTextColor(color);
                msg.status.setTextColor(color);
                break;
            case OFFER_ACCEPTED:
                    msg.actionContainer.setPivotX(100);
                    msg.actionContainer.animate()
                            .setDuration(revealDuration)
                            .alpha(0f)
                            .scaleX(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.actionContainer.setVisibility(View.GONE);
                                }
                            }).start();
                    msg.status.setPivotX(100f);
                    msg.status.animate()
                            .setDuration(revealDuration)
                            .alpha(1f)
                            .scaleX(1f)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.status.setText("Tilbud akkseptert");
                                    msg.status.setVisibility(View.VISIBLE);
                                    msg.status.setAlpha(0);
                                    msg.status.setScaleX(0);
                                }
                            }).start();

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_incoming_offer_accepted);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_accepted_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                break;
            case OFFER_DECLINED:
                    msg.actionContainer.setPivotX(100f);
                    msg.actionContainer.animate()
                            .setDuration(revealDuration)
                            .alpha(0f)
                            .scaleX(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.actionContainer.setVisibility(View.GONE);
                                }
                            }).start();
                    msg.status.setPivotX(0);
                    msg.status.animate()
                            .setDuration(revealDuration)
                            .alpha(1f)
                            .scaleX(1f)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    msg.status.setText("Tilbud avslått");
                                    msg.status.setVisibility(View.VISIBLE);
                                    msg.status.setAlpha(0f);
                                    msg.status.setScaleX(0f);
                                }
                            }).start();

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_incoming_offer_declined);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_declined_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                break;
            case OFFER_COMPLETE:
                    if(msg.actionContainer.getVisibility() == View.VISIBLE){
                        msg.actionContainer.setPivotY(100f);
                        msg.actionContainer.animate()
                                .setDuration(revealDuration)
                                .alpha(0f)
                                .scaleY(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.actionContainer.setVisibility(View.GONE);
                                    }
                                }).start();
                    }
                    if(msg.actionContainer.getVisibility() == View.GONE){
                        msg.status.setPivotY(0);
                        msg.status.animate()
                                .setDuration(revealDuration)
                                .alpha(1f)
                                .scaleY(1f)
                                .withStartAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.status.setVisibility(View.VISIBLE);
                                        msg.status.setAlpha(0);
                                        msg.status.setScaleY(0);
                                        msg.status.setText("Tilbud ferdig");
                                    }
                                }).start();
                    }

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_incoming_offer_complete);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_complete_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                break;
            case OFFER_WITHDRAWN: {
                    if (msg.actionContainer.getVisibility() == View.VISIBLE) {
                        msg.actionContainer.setPivotY(100f);
                        msg.actionContainer.animate()
                                .setDuration(revealDuration)
                                .alpha(0f)
                                .scaleY(0)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.actionContainer.setVisibility(View.GONE);
                                    }
                                }).start();
                    }
                    if (msg.status.getVisibility() == View.GONE) {
                        msg.status.setPivotY(0);
                        msg.status.animate()
                                .setDuration(revealDuration)
                                .alpha(1f)
                                .scaleY(1f)
                                .withStartAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        msg.status.setVisibility(View.VISIBLE);
                                        msg.status.setAlpha(0);
                                        msg.status.setScaleY(0);
                                        msg.status.setText("Tilbud trekkt");
                                    }
                                }).start();
                    }

                    msg.bubble.setBackgroundResource(R.drawable.chat_message_incoming_offer_complete);

                    color = ContextCompat.getColor(activity, R.color.chat_offer_message_withdrawn_text);
                    msg.offerTitle.setTextColor(color);
                    msg.message.setTextColor(color);
                    msg.status.setTextColor(color);
                break;
            }
        }
        bindMessage(msg, data, position);
    }

    private void bindMessage(final BaseMessage msg, final PQMessage data, final int position){
        runEnterAnimation(msg.itemView, position);
        msg.time.setText(data.added.toString("dd. MMM HH:mm"));

        final boolean isExpanded = sentTimeIndex == position;
        msg.timeContainer.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        msg.timeContainer.setActivated(isExpanded);

        msg.bubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPos = sentTimeIndex;
                sentTimeIndex = isExpanded ? -1:position;
                TransitionManager.beginDelayedTransition(recyclerView,
                        TransitionInflater.from(activity).inflateTransition(R.transition.message_time));
                notifyItemChanged(sentTimeIndex);
                if(oldPos != -1) notifyItemChanged(oldPos);
            }
        });

        msg.date.setText(data.added.toString("d. MMMM"));

        if(position == 0){
            msg.date.setVisibility(View.VISIBLE);
        } else {
            int start = getItem(position -1).added.getDayOfYear();
            if(start < data.added.getDayOfYear()){
                msg.date.setVisibility(View.VISIBLE);
            } else {
                msg.date.setVisibility(View.GONE);
            }
        }

        if(data.hasBody){
            msg.message.setVisibility(View.GONE);
        } else {
            msg.message.setText(data.body);
        }

    }
    private void bindChatMessage(final ChatMessage msg, final PQMessage data, int position){
        if(data.hasImage){
            msg.image.setVisibility(View.VISIBLE);

            ImageLoader loader = new ImageLoader(activity, msg);
            loader.execute(data.id);
            msg.image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            msg.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater inflater = activity.getLayoutInflater();
                    View imageDialog = inflater.inflate(R.layout.image_display, null);
                    Dialog dialog = new Dialog(activity, R.style.DisplayDialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(imageDialog);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DisplayDialogAnimation;

                    SubsamplingScaleImageView imgImage = (SubsamplingScaleImageView) imageDialog.findViewById(R.id.display_image);
                    imgImage.setImage(ImageSource.bitmap(PQDataHandler.getInstance(activity).getImageLarge(data.id)));

                    dialog.show();
                }
            });

        } else {
            msg.image.setVisibility(View.GONE);
        }
    }
    private void bindOutgoingMessage(final OutgoingMessage msg, final PQMessage data, final int position){

        final boolean isVisible = position == deleteIndex;
        msg.delete.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        msg.delete.setActivated(isVisible);
        msg.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PQServer.deleteMessage(data.id, deleteCallback, activity);
            }
        });
        msg.bubble.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TransitionManager.beginDelayedTransition(recyclerView,
                        TransitionInflater.from(activity).inflateTransition(R.transition.message_time));
                oldPos = deleteIndex;
                deleteIndex = isVisible ? -1 : position;
                notifyItemChanged(deleteIndex);
                if(oldPos != -1) notifyItemChanged(oldPos);
                return true;
            }
        });
        bindMessage(msg, data, position);
        bindChatMessage(msg, data, position);
    }
    private void bindIncomingMessage(IncomingMessage msg, final PQMessage data, int position){
        msg.sender.setText(data.senderName);
        if (senderNameVisible) {
            msg.sender.setAlpha(0f);
            msg.sender.setVisibility(View.VISIBLE);
            msg.sender.animate()
                    .alpha(1f)
                    .setDuration(revealDuration)
                    .setInterpolator(new DecelerateInterpolator())
                    .start();
        } else {
            msg.sender.setVisibility(View.GONE);
        }
        bindMessage(msg, data, position);
        bindChatMessage(msg, data, position);
    }

    @Override
    public void onBindData(BaseMessage holder, PQMessage data, int viewType, int position) {
        switch (viewType){
            case MESSAGE_TYPE_INCOMING:
                bindIncomingMessage((IncomingMessage)holder, data, position);
                break;
            case MESSAGE_TYPE_INCOMING_OFFER:
                bindIncomingOffer((IncomingOfferMessage)holder, data, position);
                break;
            case MESSAGE_TYPE_OUTGOING:
                bindOutgoingMessage((OutgoingMessage)holder, data, position);
                break;
            case MESSAGE_TYPE_OUTGOING_OFFER:
                bindOutgoingOffer((OutgoingOfferMessage)holder, data, position);
                break;
        }
    }

    private class ImageLoader extends AsyncTask<Integer, Integer, Bitmap> {

        private ChatMessage message;
        private ValueAnimator animator;
        private Activity activity;

        public ImageLoader(Activity activity, final ChatMessage message) {
            this.message = message;
            this.activity = activity;
            this.animator = ValueAnimator.ofArgb(Color.MAGENTA, Color.GREEN, Color.RED, Color.YELLOW);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    message.image.setBackgroundColor((int)animator.getAnimatedValue());
                }
            });
            animator.setDuration(4000);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.setRepeatCount(Animation.INFINITE);
        }

        @Override
        protected void onPreExecute() {
            animator.start();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(bitmap != null) message.image.setImageBitmap(bitmap);
            else message.image.setVisibility(View.GONE);
            animator.end();
            Toast.makeText(activity, R.string.error_loading_image, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            PQDataHandler db = PQDataHandler.getInstance(activity);
            int id = params[0];
            Bitmap image = db.getImageSmall(id);
            if(image == null){
                byte[] imageData = PQServer.getImage(id, activity);
                if(imageData != null){
                    db.addImage(id, imageData);
                    image = db.getImageSmall(id);
                }
            }
            return image;
        }
    }

    private PQServer.Callback<PQInfo> deleteCallback = new PQServer.Callback<PQInfo>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(activity, R.string.error_deleting_message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQInfo response) {
            removeItemById(response.id);
            Toast.makeText(activity, R.string.chat_message_deleted_success, Toast.LENGTH_SHORT).show();
        }
    };

    private PQServer.Callback<PQOffer> withdrawOfferCallback = new PQServer.Callback<PQOffer>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(activity, R.string.error_deleting_message, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQOffer response) {
            Log.d(TAG, "Deleting message " + response.id);
            Toast.makeText(activity, R.string.chat_message_deleted_success, Toast.LENGTH_SHORT).show();
            updateOfferForItem(response);
        }
    };

    private PQServer.Callback<PQOffer> acceptOfferCallback = new PQServer.Callback<PQOffer>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(activity, R.string.error_offer_accept, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQOffer response) {
            Log.d(TAG, "Response " + response.id + " accepted");
            Toast.makeText(activity, R.string.chat_offer_accepted_success, Toast.LENGTH_SHORT).show();
            updateOfferForItem(response);
        }
    };

    private PQServer.Callback<PQOffer> declineOfferCallback = new PQServer.Callback<PQOffer>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(activity, R.string.error_offer_accept, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQOffer response) {
            Log.d(TAG, "Response " + response.id + " declined");
            Toast.makeText(activity, R.string.chat_offer_declined_success, Toast.LENGTH_SHORT).show();
            updateOfferForItem(response);
        }
    };

    public void updateOfferForItem(PQOffer offer){
        PQMessage msg = getItemByOfferId(offer.id);
        msg.offer = offer;

        for(int i = 0; i < getItemCount(); i++){
            PQMessage toUpdate = getItem(i);
            if(toUpdate.hasOffer){
                if(toUpdate.offer.id == offer.id){
                    updateItem(i, msg);
                    return;
                }
            }
        }
    }

    public PQMessage getItemByOfferId(int offerId){
        for(int i = 0; i < getItemCount(); i++){
            PQMessage toReturn = getItem(i);
            if(toReturn.hasOffer){
                if(toReturn.offer.id == offerId){
                    return toReturn;
                }
            }
        }
        return null;
    }

    public void removeItemById(int messageId){
        for(int i = 0; i < getItemCount(); i++){
            PQMessage toDelete = getItem(i);
            if(toDelete.id == messageId){

                removeItem(toDelete);
                return;
            }
        }
    }

    public void removeItemByOfferId(int offerId){
        for(int i = 0; i < getItemCount(); i++){
            PQMessage msg = getItem(i);
            if(msg.offer.id == offerId){
                removeItem(msg);
                return;
            }
        }
    }
}
