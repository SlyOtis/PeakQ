package com.slyotis.peakq.chat_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 20.11.16.
 */

public class OfferMessage extends BaseMessage{

    public TextView offerTitle;
    public TextView amount;
    public TextView status;

    public OfferMessage(View itemView) {
        super(itemView);
        offerTitle = (TextView) itemView.findViewById(R.id.chat_offer_message_text);
        amount = (TextView) itemView.findViewById(R.id.chat_offer_message_amount);
        status = (TextView) itemView.findViewById(R.id.chat_offer_message_status);
    }
}
