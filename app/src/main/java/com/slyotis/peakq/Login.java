package com.slyotis.peakq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.slyotis.peakq.login_pager.LoginPagerAdapter;
import com.slyotis.peakq.views.VerticalViewPager;

/**
 * A login screen that offers login via email/password.
 */
public class Login extends AppCompatActivity{

    // Data for main activity
    public static final String USER_DATA = "user_data";

    public static VerticalViewPager pager;
    private LoginPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pager = (VerticalViewPager)findViewById(R.id.login_pager);
        adapter = new LoginPagerAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if(pager.getCurrentItem() == 1){
            pager.setCurrentItem(0, true);
        } else super.onBackPressed();
    }
}

