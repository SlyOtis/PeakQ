package com.slyotis.peakq;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.slyotis.peakq.camera.AutoFitTextureView;
import com.slyotis.peakq.camera.CameraCompatActivity;

/**
 * Created by slyotis on 15.11.16.
 */

public class Camera extends CameraCompatActivity {

    @Override
    public AutoFitTextureView onCreateCamera(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_camera);
        return (AutoFitTextureView) findViewById(R.id.camera_view);
    }
}
