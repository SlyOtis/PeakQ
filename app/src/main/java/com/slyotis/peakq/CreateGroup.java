package com.slyotis.peakq;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.main_pager.groups_recycler.GroupsGroups;

import org.joda.time.DateTime;


/**
 * Created by slyotis on 14.11.16.
 */

public class CreateGroup extends AppCompatActivity {

    private static final String TAG = CreateGroup.class.getName();

    private EditText mTitle;
    private TextInputEditText mDescription;
    private TextView privateText;
    private TextView notificationsText;
    private ImageView privateIcon;
    private ImageView notificationsIcon;

    private boolean isPrivate = false;
    private boolean getPush = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        setSupportActionBar((Toolbar)findViewById(R.id.create_group_toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.create_group_title);

        mTitle = (EditText)findViewById(R.id.create_title);
        mDescription = (TextInputEditText)findViewById(R.id.create_description);
        privateText = (TextView)findViewById(R.id.action_private_text);
        privateIcon = (ImageView)findViewById(R.id.action_private_icon);
        notificationsText = (TextView)findViewById(R.id.action_notifications_text);
        notificationsIcon = (ImageView)findViewById(R.id.action_notifications_icon);

        findViewById(R.id.action_private).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPrivate){
                    privateText.setText(R.string.create_group_action_private_private);
                    privateIcon.setImageResource(R.drawable.ic_people_white_24dp);
                    isPrivate = false;
                } else {
                    privateText.setText(R.string.create_group_action_private_public);
                    privateIcon.setImageResource(R.drawable.ic_person_white_24dp);
                    isPrivate = true;
                }
            }
        });

        findViewById(R.id.action_notifications).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getPush){
                    notificationsText.setText(R.string.create_group_action_notifications_on);
                    notificationsIcon.setImageResource(R.drawable.ic_notifications_active_white_24dp);
                    getPush = false;
                } else {
                    notificationsText.setText(R.string.create_group_action_notifications_off);
                    notificationsIcon.setImageResource(R.drawable.ic_notifications_off_white_24dp);
                    getPush = true;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.create_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_create_group){
            create();
        } else if(item.getItemId() == android.R.id.home){
            setResult(Activity.RESULT_CANCELED);
            finishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    private void create(){
        String title = mTitle.getText().toString();
        String description = mDescription.getText().toString();

        mTitle.setError(null);
        mDescription.setError(null);

        if(TextUtils.isEmpty(title)){
            mTitle.setError(getString(R.string.create_group_error_missing_title));
            mTitle.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(description)){
            mDescription.setError(getString(R.string.create_group_error_missing_description));
            mDescription.requestFocus();
            return;
        }

        Intent data = new Intent();
        data.putExtra(GroupsGroups.CREATE_GROUP_DATA, new PQGroup(0, title, description, isPrivate, true, true, getPush, DateTime.now()));
        setResult(Activity.RESULT_OK, data);
        finishAfterTransition();
    }
}
