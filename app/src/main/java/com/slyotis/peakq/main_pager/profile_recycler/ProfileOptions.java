package com.slyotis.peakq.main_pager.profile_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.slyotis.peakq.R;

import java.io.Serializable;

/**
 * Created by slyotis on 03.12.16.
 */

public class ProfileOptions extends RecyclerView.ViewHolder{
    public TextView optionTitle;
    public ImageView optionIcon;

    public ProfileOptions(View itemView) {
        super(itemView);
        optionIcon = (ImageView) itemView.findViewById(R.id.profile_option_icon);
        optionTitle = (TextView) itemView.findViewById(R.id.profile_option_text);
    }
}
