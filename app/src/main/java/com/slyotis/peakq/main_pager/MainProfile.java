package com.slyotis.peakq.main_pager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.slyotis.peakq.Login;
import com.slyotis.peakq.R;
import com.slyotis.peakq.behaviour.DividerItemDecoration;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQInfo;
import com.slyotis.peakq.data.PQOffer;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.firebase.FBUser;
import com.slyotis.peakq.firebase.PQFire;
import com.slyotis.peakq.main_pager.profile_recycler.ProfileArrayAdapter;
import com.slyotis.peakq.main_pager.profile_recycler.ProfileOptionsDecorator;
import com.slyotis.peakq.services.PQIdService;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import static com.slyotis.peakq.services.PQNotificationsService.ACTION_DELETE_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.ACTION_NEW_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.CATEGORY_OFFER;
import static com.slyotis.peakq.services.PQNotificationsService.OFFER_DATA;

/**
 * Created by slyotis on 17.09.16.
 */
public class MainProfile extends PageFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = MainProfile.class.getName();

    private RecyclerView recycler;
    private LinearLayoutManager layoutManager;
    private ProfileArrayAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    private int defaultOfferCount = 20;
    private int index = 20;

    private FBUser user;

    public static MainProfile newInstance(){
        return new MainProfile();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.main_pager_profile, container, false);
        recycler = (RecyclerView)root.findViewById(R.id.profile_recycler);
        // To increase performance
        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);
        adapter = new ProfileArrayAdapter(getActivity(), recycler);
        recycler.setAdapter(adapter);
        ProfileOptionsDecorator decorator = new ProfileOptionsDecorator(getActivity());
        recycler.addItemDecoration(decorator);

        refreshLayout = (SwipeRefreshLayout)root.findViewById(R.id.profile_refresh);
        refreshLayout.setOnRefreshListener(this);

        final TextView mEmail = (TextView)root.findViewById(R.id.profile_header_email);
        final TextView mPhone = (TextView)root.findViewById(R.id.profile_header_phone);
        final TextView mUsername = (TextView)root.findViewById(R.id.profile_header_username);
        final TextView mCredits = (TextView)root.findViewById(R.id.profile_header_credits);

        root.findViewById(R.id.profile_action_sign_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });

        // TODO:: FIX states
        if(savedInstanceState != null){
            FBUser user = (FBUser)savedInstanceState.getSerializable(PQFire.User.OBJECT_NAME);
            if(user != null) {
                Log.d(TAG, "HELLO");
                mEmail.setText(user.email);
                mPhone.setText(user.phone);
                mUsername.setText(user.firstName + " " + user.lastName);
                mCredits.setText(String.valueOf(user.credits));
            }
        }

        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(PQFire.TABLE_USERS + "/" + id);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(FBUser.class);

                if(user == null) {
                    signOut();
                    return;
                }

                mEmail.setText(user.email);
                mPhone.setText(user.phone);
                mUsername.setText(user.firstName + " " + user.lastName);
                mCredits.setText(String.valueOf(user.credits));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                signOut();
            }
        });


        return root;
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        //getOffers(defaultOfferCount, 0);
    }

    private void signOut(){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signOut();

        Intent intent = new Intent(getActivity(), Login.class);
        startActivity(intent);
        getActivity().finish();
    }
}
