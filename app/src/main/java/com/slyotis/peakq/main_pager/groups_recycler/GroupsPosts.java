package com.slyotis.peakq.main_pager.groups_recycler;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.slyotis.peakq.CreatePost;
import com.slyotis.peakq.R;
import com.slyotis.peakq.behaviour.DividerItemDecoration;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.services.PQNotificationsService;
import com.slyotis.peakq.views.FabulousActionButton;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.ReadablePartial;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import static com.slyotis.peakq.services.PQNotificationsService.ACTION_DELETE_POST;
import static com.slyotis.peakq.services.PQNotificationsService.ACTION_NEW_POST;
import static com.slyotis.peakq.services.PQNotificationsService.CATEGORY_POST;
import static com.slyotis.peakq.services.PQNotificationsService.POST_DATA;

/**
 * Created by slyotis on 24.10.16.
 */

public class GroupsPosts extends PageFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    // For debugging purposes
    private static final String TAG = GroupsPosts.class.getName();
    private static final int CREATE_POST = 2;

    public static final String CREATE_POST_DATA = "create_post_data";

    private RecyclerView recycler;
    private LinearLayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;
    private FabulousActionButton fab;

    private PostsArrayAdapter adapter;
    private PQGroup group;

    private int defaultPostCount = 20;
    private int index = 0;

    public static GroupsPosts newInstance(PQGroup group){
        GroupsPosts posts = new GroupsPosts();
        posts.group = group;
        return posts;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.groups_posts, container, false);

        refreshLayout = (SwipeRefreshLayout)root.findViewById(R.id.groups_posts_refresh);
        refreshLayout.setOnRefreshListener(this);

        recycler = (RecyclerView)root.findViewById(R.id.groups_posts);
        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);

        adapter = new PostsArrayAdapter(getActivity());
        recycler.setAdapter(adapter);

        fab = (FabulousActionButton) root.findViewById(R.id.posts_fab);
        fab.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), CreatePost.class);
        intent.putExtra(CreatePost.GROUP_TITLE, group.name);
        startActivityForResult(intent, CREATE_POST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CREATE_POST){
            if(resultCode == Activity.RESULT_OK){
                PQPost post = (PQPost)data.getSerializableExtra(CREATE_POST_DATA);
                if(post != null){
                    // TODO : get image data is exist
                    PQServer.createPost(group.id, post.expires.toString(),
                            post.title, post.description, null, createCallback, getActivity());
                } else {
                    Log.d(TAG, "Missing post data from create");
                }
            }

        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private PQServer.Callback<PQPost> createCallback = new PQServer.Callback<PQPost>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
            Toast.makeText(getActivity(), R.string.create_post_error_create, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int requestId, PQPost response) {
            Toast.makeText(getActivity(), R.string.create_post_complete, Toast.LENGTH_SHORT).show();
            adapter.addItem(response);
        }
    };

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(adapter.getItemCount() == 0){
            onRefresh();
        }
    }

    public void loadPosts(int count, int start){
        PQServer.getPosts(count, start, group.id, postsCallback, getActivity());
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        loadPosts(defaultPostCount, 0);
    }


    private PQServer.Callback<PQPost[]> postsCallback = new PQServer.Callback<PQPost[]>() {
        @Override
        public void onFailure(int errorCode, String message) {
            refreshLayout.setRefreshing(false);
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQPost[] response) {
            refreshLayout.setRefreshing(false);
            PostCheck postCheck = new PostCheck();
            postCheck.execute(response);
        }
    };

    private class PostCheck extends AsyncTask<PQPost[], PQPost, PQPost[]>{

        @Override
        protected PQPost[] doInBackground(PQPost[]... params) {
            for(final PQPost post : params[0]){
                post.expires = post.expires.withSecondOfMinute(0);
                if(post.added.isAfter(post.expires)) continue;
                boolean notExpired = new Interval(post.added, post.expires).contains(DateTime.now());
                if(!post.solved && notExpired) {
                    publishProgress(post);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(final PQPost... values) {
            super.onProgressUpdate(values);
            adapter.addItem(values[0]);
        }
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addCategory(CATEGORY_POST);
        filter.addAction(ACTION_DELETE_POST);
        filter.addAction(ACTION_NEW_POST);
        getActivity().registerReceiver(postReceiver, filter);
        super.onResume();
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(postReceiver);
        super.onPause();
    }

    private BroadcastReceiver postReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case ACTION_DELETE_POST:{
                    int id = intent.getIntExtra(POST_DATA, -1);
                    if(id > 0) adapter.removeItemById(intent.getIntExtra(POST_DATA, -1));
                    break;
                }
                case ACTION_NEW_POST:{
                    adapter.addItem((PQPost)intent.getSerializableExtra(POST_DATA));
                    break;
                }
            }
        }
    };
}
