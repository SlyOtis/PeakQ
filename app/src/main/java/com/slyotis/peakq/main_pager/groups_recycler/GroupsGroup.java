package com.slyotis.peakq.main_pager.groups_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 21.09.16.
 */

public class GroupsGroup extends RecyclerView.ViewHolder{

    public int revealDuration = 300;

    public TextView date;
    public TextView description;
    public TextView title;
    public ImageView push;
    public ImageView pushFull;
    public ImageView member;
    public ImageView memberFull;
    public ImageView edit;
    public ImageView editFull;
    public View more;
    public TextView pushText;
    public TextView memberText;
    public TextView editText;
    public TextView memberInfo;
    public View options;
    public View optionsFull;
    public View root;
    public View memberBtn;
    public View pushBtn;
    public View editBtn;
    public View indicator;

    public GroupsGroup(View itemView) {
        super(itemView);
        date = (TextView)itemView.findViewById(R.id.groups_group_date);
        description = (TextView)itemView.findViewById(R.id.groups_group_description);
        title = (TextView)itemView.findViewById(R.id.groups_group_title);
        memberInfo = (TextView)itemView.findViewById(R.id.groups_group_member);
        root = itemView.findViewById(R.id.groups_group_info);
        push = (ImageView)itemView.findViewById(R.id.groups_group_action_push);
        pushFull = (ImageView)itemView.findViewById(R.id.groups_group_action_push_icon);
        options = itemView.findViewById(R.id.groups_group_options_small);
        optionsFull = itemView.findViewById(R.id.groups_group_options_full);
        member = (ImageView) itemView.findViewById(R.id.groups_group_action_member);
        memberFull = (ImageView) itemView.findViewById(R.id.groups_group_action_member_icon);
        edit = (ImageView) itemView.findViewById(R.id.groups_group_action_edit);
        editFull = (ImageView)itemView.findViewById(R.id.groups_group_action_edit_icon);
        more = itemView.findViewById(R.id.groups_group_action_more);
        pushText = (TextView) itemView.findViewById(R.id.groups_group_action_push_text);
        memberText = (TextView) itemView.findViewById(R.id.groups_group_action_member_text);
        editText = (TextView) itemView.findViewById(R.id.groups_group_action_edit_text);
        memberBtn = itemView.findViewById(R.id.groups_group_action_member_full);
        pushBtn = itemView.findViewById(R.id.groups_group_action_push_full);
        editBtn = itemView.findViewById(R.id.groups_group_action_edit_full);
        indicator = itemView.findViewById(R.id.groups_group_member_indicator);
    }

    public void isAdmin(boolean admin){
        if(admin){
            editBtn.setVisibility(View.VISIBLE);
            edit.setVisibility(View.VISIBLE);
            push.setVisibility(View.VISIBLE);
            pushBtn.setVisibility(View.VISIBLE);
            memberInfo.setText(R.string.groups_group_admin_text);
            indicator.setBackgroundResource(R.color.groups_group_member_indicator_admin);
        } else {
            pushBtn.setVisibility(View.GONE);
            editBtn.setVisibility(View.GONE);
            memberInfo.setText("");
            indicator.setBackgroundResource(R.color.groups_group_member_indicator_default);
        }
    }

    public void isMember(boolean isMember){
        if(isMember){
            editBtn.setVisibility(View.GONE);
            push.setVisibility(View.VISIBLE);
            pushBtn.setVisibility(View.VISIBLE);
            memberInfo.setText(R.string.groups_group_member_text);
            indicator.setBackgroundResource(R.color.groups_group_member_indicator_member);
        } else {
            pushBtn.setVisibility(View.GONE);
            editBtn.setVisibility(View.GONE);
            push.setVisibility(View.GONE);
            memberInfo.setText("");
            indicator.setBackgroundResource(R.color.groups_group_member_indicator_default);

        }
    }

    public void showOptionsFull(boolean show){
        if(show){
            more.post(new Runnable() {
                @Override
                public void run() {
                    more.setPivotX(more.getWidth()/2);
                    more.setPivotY(more.getHeight()/2);
                    more.animate()
                            .setDuration(revealDuration)
                            .rotation(180f)
                            .start();
                }
            });
            optionsFull.setVisibility(View.VISIBLE);
            options.setVisibility(View.GONE);
        } else {
            more.post(new Runnable() {
                @Override
                public void run() {
                    more.setPivotX(more.getWidth()/2);
                    more.setPivotY(more.getHeight()/2);
                    more.animate()
                            .setDuration(revealDuration)
                            .rotation(0f)
                            .start();
                }
            });


            optionsFull.setVisibility(View.GONE);
            options.setVisibility(View.VISIBLE);
        }
    }

    public void setPushOn(boolean state){
        if(state){
            push.setImageResource(R.drawable.group_btn_push_on);
            pushFull.setImageResource(R.drawable.group_btn_push_off);
            pushText.setText(R.string.groups_group_action_push_off);
        } else {
            push.setImageResource(R.drawable.group_btn_push_off);
            pushFull.setImageResource(R.drawable.group_btn_push_on);
            pushText.setText(R.string.groups_group_action_push_on);
        }
    }

    public void setIsMember(boolean state){
        if(state){
            member.setImageResource(R.drawable.group_btn_member_leave);
            memberFull.setImageResource(R.drawable.group_btn_member_leave);
            memberText.setText(R.string.groups_group_action_leave);
        } else {
            member.setImageResource(R.drawable.group_btn_member_join);
            memberFull.setImageResource(R.drawable.group_btn_member_join);
            memberText.setText(R.string.groups_group_action_join);
        }
    }
}
