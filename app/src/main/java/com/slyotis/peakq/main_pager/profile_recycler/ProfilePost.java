package com.slyotis.peakq.main_pager.profile_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slyotis.peakq.R;

import org.w3c.dom.Text;

/**
 * Created by slyotis on 18.09.16.
 */
@Deprecated
public class ProfilePost extends RecyclerView.ViewHolder{
    public TextView title;
    public TextView date;
    public TextView group;
    public TextView description;
    public TextView remaining;
    public View root;
    public ProfilePost(View itemView) {
        super(itemView);
        date = (TextView)itemView.findViewById(R.id.profile_post_date);
        title = (TextView)itemView.findViewById(R.id.profile_post_title);
        group = (TextView)itemView.findViewById(R.id.profile_post_group);
        description = (TextView)itemView.findViewById(R.id.profile_post_description);
        remaining = (TextView)itemView.findViewById(R.id.profile_post_timer);
        root = itemView.findViewById(R.id.profile_post);
    }
}
