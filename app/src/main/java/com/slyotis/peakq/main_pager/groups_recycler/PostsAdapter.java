package com.slyotis.peakq.main_pager.groups_recycler;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.slyotis.peakq.Chat;
import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerViewCursorAdapter;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServerOld;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

/**
 * Created by slyotis on 24.10.16.
 */

@Deprecated
public class PostsAdapter extends RecyclerViewCursorAdapter<PQPost, GroupsPost> {

    private Activity activity;

    public PostsAdapter(Activity activity, @NonNull Class<PQPost> klass, @Nullable Cursor cursor) {
        super(klass, cursor);
        this.activity = activity;
    }

    @Override
    public PQPost fromCursorRow(Cursor cursor) {
        return PQDataHandler.getInstance(activity).getPostFromRow(cursor);
    }

    @Override
    public GroupsPost onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsPost(LayoutInflater.from(activity)
                .inflate(R.layout.groups_post, parent, false));
    }

    @Override
    public void onBindViewHolder(final GroupsPost post, int position) {
        final PQPost data = getItem(position);
        post.date.setText(data.added.toString());
        post.author.setText(data.authorName);
        post.description.setText(data.authorName);
        post.timer.setText("14");
        post.title.setText(data.title);
        //post.image.setImageBitmap(null);

        if(data.hasImage){
          //  post.image.setVisibility(View.VISIBLE);
            post.timerContainer.setVisibility(View.GONE);

            ImageLoader loader = new ImageLoader(activity, post);

            loader.execute(data.id);
             /*
            post.image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
           post.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater inflater = activity.getLayoutInflater();
                    View imageDialog = inflater.inflate(R.layout.image_display, null);
                    Dialog dialog = new Dialog(activity, R.style.DisplayDialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(imageDialog);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DisplayDialogAnimation;

                    SubsamplingScaleImageView imgImage = (SubsamplingScaleImageView) imageDialog.findViewById(R.id.display_image);
                    imgImage.setImage(ImageSource.bitmap(PQDataHandler.getInstance(activity).getImageLarge(data.id)));

                    dialog.show();
                }
            }); */

        } else {
           // post.image.setVisibility(View.GONE);
            post.timerContainer.setVisibility(View.VISIBLE);
        }

        post.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Chat.class);
                //intent.putExtra(POST_DATA, data);
                activity.startActivity(intent);
            }
        });
    }

    private class ImageLoader extends AsyncTask<Integer, Integer, Bitmap>{

        private GroupsPost post;
        private ValueAnimator animator;
        private Activity activity;

        public ImageLoader(Activity activity, final GroupsPost post) {
            this.post = post;
            this.activity = activity;
            this.animator = ValueAnimator.ofArgb(Color.MAGENTA, Color.GREEN, Color.RED, Color.YELLOW);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    //post.image.setBackgroundColor((int)animator.getAnimatedValue());
                }
            });
            animator.setDuration(4000);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.setRepeatCount(Animation.INFINITE);
        }

        @Override
        protected void onPreExecute() {
            animator.start();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //post.image.setImageBitmap(bitmap);
            animator.end();
            //bitmap.recycle();
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            PQDataHandler db = PQDataHandler.getInstance(activity);
            int id = params[0];
            Bitmap image = null;
            image = db.getImageSmall(id);
            if(image == null){
               // db.addImage(id, BitmapFactory.decodeStream(PQServerOld.getImage(id).body().byteStream()));
                image = db.getImageSmall(id);
            }
            return image;
        }
    }

    private void revealShow(View rootView, boolean reveal, final Dialog dialog) {
        final View view = rootView.findViewById(R.id.chat_toolbar);
        int w = view.getWidth();
        int h = view.getHeight();
        float maxRadius = (float) Math.sqrt(w * w / 4 + h * h / 4);

        if(reveal){
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view,
                    w / 2, h / 2, 0, maxRadius);
            view.setVisibility(View.VISIBLE);
            revealAnimator.start();
        } else {
            Animator anim = ViewAnimationUtils.createCircularReveal(view, w / 2, h / 2, maxRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);

                }
            });

            anim.start();
        }
    }
}
