package com.slyotis.peakq.main_pager;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.view.ViewGroup;

import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.PageFragment;

import java.util.Locale;

/**
 * Created by slyotis on 17.09.16.
 */
public class MainPagerAdapter extends FragmentPagerAdapter{

    private static int NUM_OF_PAGES = 3;
    private Activity parent;
    private SparseArrayCompat<PageFragment> fragmentMap;
    private boolean inGroup;

    public MainPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.parent = activity;
        fragmentMap = new SparseArrayCompat<>();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                MainProfile profile = (MainProfile)fragmentMap.get(0);
                if(profile == null) {
                    profile = MainProfile.newInstance();
                    fragmentMap.put(0, profile);
                }
                return profile;
            case 1:
                MainGroups groups = (MainGroups)fragmentMap.get(1);
                if(groups == null) {
                    groups = MainGroups.newInstance();
                    fragmentMap.put(1, groups);
                }
                return groups;
            case 2:
                MainThreads messages = (MainThreads)fragmentMap.get(2);
                if(messages == null) {
                    messages = MainThreads.newInstance();
                    fragmentMap.put(2, messages);
                }
                return messages;
        }
        return null;
    }

    public void activateFragment(int index){
        for(int i = 0; i < NUM_OF_PAGES; i++){
            if(i == index) getItemAt(index).onActivate();
            else  getItemAt(index).onDeactivate();
        }
    }

    public MainGroups getGroupsFragment(){
        MainGroups groups = null;
        try{
            groups = (MainGroups)getItemAt(1);
        } catch (Exception e){
            Log.d("Error", "groups fragment not ready");
        }
        return groups;
    }

    public PageFragment getItemAt(int index){
        return fragmentMap.get(index, MainProfile.newInstance());
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        fragmentMap.remove(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position){
            case 0:
                return parent.getString(R.string.page_profile_title).toUpperCase(l);
            case 1:
                return parent.getString(R.string.page_groups_title).toUpperCase(l);
            case 2:
                return parent.getString(R.string.page_messages_title).toUpperCase(l);
        }
        return null;
    }



    @Override
    public int getCount() {
        return NUM_OF_PAGES;
    }
}
