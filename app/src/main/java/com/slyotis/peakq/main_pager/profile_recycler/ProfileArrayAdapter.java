package com.slyotis.peakq.main_pager.profile_recycler;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerArrayAdapter;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQOffer;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQUser;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

import static com.slyotis.peakq.data.PQOffer.ACCEPTED;
import static com.slyotis.peakq.data.PQOffer.COMPLETE;
import static com.slyotis.peakq.data.PQOffer.DECLINED;
import static com.slyotis.peakq.data.PQOffer.PENDING;

/**
 * Created by slyotis on 18.09.16.
 */
public class ProfileArrayAdapter extends RecyclerArrayAdapter<RecyclerView.ViewHolder, PQOffer>
        implements View.OnClickListener{

    private static final String TAG = ProfileArrayAdapter.class.getName();

    private static final int TYPE_OFFER = 0;
    private static final int TYPE_DIVIDER = 1;
    private static final int TYPE_OPTION = 2;

    private static final int ACTION_GROUPS = 1;
    private static final int ACTION_POSTS = 2;
    private static final int ACTION_CREDIT = 3;

    private Activity activity;
    private RecyclerView recyclerView;
    private PQUser user;

    public ProfileArrayAdapter(Activity activity, RecyclerView recyclerView) {
        this.activity = activity;
        this.recyclerView = recyclerView;
        this.user = PQDataHandler.getCurrentUser(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case TYPE_OPTION:
                return new ProfileOptions(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.profile_option, parent, false));
            case TYPE_DIVIDER:
                return new ProfileOptionsDivider(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.profile_options_divider, parent, false));
            default:
                return new ProfileOffer(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.profile_offer, parent, false));
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 4;
    }

    @Override
    public int getItemViewType(int position) {
        if(position > getDataCount() -1 ){
            if(position - getDataCount() == 0) return TYPE_DIVIDER;
            else return TYPE_OPTION;
        }
        return TYPE_OFFER;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProfileOffer) bindOffer((ProfileOffer) holder, getItem(position), position);
        else if(holder instanceof ProfileOptions){
            ProfileOptions opt = (ProfileOptions)holder;
            switch (position - getDataCount()){
                case (ACTION_POSTS):{
                    opt.optionTitle.setText(R.string.profile_options_action_posts);
                    opt.optionIcon.setImageResource(R.drawable.ic_group_white_24dp);
                    opt.optionIcon.setColorFilter(ContextCompat.getColor(activity, R.color.colorPost));
                    opt.itemView.setOnClickListener(this);
                    opt.itemView.setTag(ACTION_POSTS);
                    break;
                }
                case (ACTION_CREDIT):{
                    opt.optionTitle.setText(R.string.profile_options_action_credit);
                    opt.optionIcon.setImageResource(R.drawable.ic_attach_money_white_24dp);
                    opt.optionIcon.setColorFilter(ContextCompat.getColor(activity, R.color.colorCredit));
                    opt.itemView.setOnClickListener(this);
                    opt.itemView.setTag(ACTION_CREDIT);
                    break;
                }
                case (ACTION_GROUPS):{
                    opt.optionTitle.setText(R.string.profile_options_action_groups);
                    opt.optionIcon.setImageResource(R.drawable.ic_people_white_24dp);
                    opt.optionIcon.setColorFilter(ContextCompat.getColor(activity, R.color.colorGroup));
                    opt.itemView.setOnClickListener(this);
                    opt.itemView.setTag(ACTION_GROUPS);
                    break;
                }
            }
        }
    }

    private void bindOffer(final ProfileOffer offer, final PQOffer data, final  int position){
        offer.post.setText(data.postTitle);

        Interval interval = new Interval(DateTime.now(), data.expires);
        Period period = interval.toPeriod();

        if(period.getDays() < 1){
            if(period.getHours() <= 0) {
                offer.timerType.setText(R.string.groups_post_timer_remaining_minutes);
                offer.timer.setText(String.valueOf(period.getMinutes()));
                offer.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_minutes);
            } else {
                offer.timerType.setText(R.string.groups_post_timer_remaining_hours);
                offer.timer.setText(String.valueOf(period.getHours()));
                offer.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_hours);
            }
        } else {
            offer.timerType.setText(R.string.groups_post_timer_remaining_days);
            offer.timer.setText(String.valueOf(period.getDays()));
            if(period.getDays() > 31){
                offer.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_months);
            } else if(period.getDays() > 7){
                offer.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_weeks);
            } else{
                offer.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_days);
            }
        }

        switch (data.status){
            case PENDING: {
                if (user.id != data.senderId) {
                    offer.data.setText(data.senderName + " tilbyr deg");
                    offer.acceptPanel.setVisibility(View.VISIBLE);
                    offer.accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            offer.accept.setEnabled(false);
                            PQServer.acceptOffer(data.id, new PQServer.Callback<PQOffer>() {
                                @Override
                                public void onFailure(int errorCode, String message) {
                                    Log.d(TAG, errorCode + ", " + message);
                                    offer.accept.setEnabled(true);
                                }

                                @Override
                                public void onSuccess(int requestId, PQOffer response) {
                                    offer.accept.setEnabled(true);
                                    TransitionManager.beginDelayedTransition(recyclerView,
                                            TransitionInflater.from(activity).inflateTransition(R.transition.offer_status));
                                    updateItem(position, response);
                                }
                            }, activity);
                        }
                    });
                    offer.decline.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            offer.decline.setEnabled(false);
                            PQServer.declineOffer(data.id, new PQServer.Callback<PQOffer>() {
                                @Override
                                public void onFailure(int errorCode, String message) {
                                    Log.d(TAG, errorCode + ", " + message);
                                    offer.decline.setEnabled(true);
                                }

                                @Override
                                public void onSuccess(int requestId, PQOffer response) {
                                    offer.decline.setEnabled(true);
                                    TransitionManager.beginDelayedTransition(recyclerView,
                                            TransitionInflater.from(activity).inflateTransition(R.transition.offer_status));
                                    updateItem(position, response);
                                }
                            }, activity);
                        }
                    });
                } else {
                    offer.data.setText("Venter på at ? skal akkseptere ");
                    offer.acceptPanel.setVisibility(View.GONE);
                }
                break;
            }
            case ACCEPTED:{
                offer.root.setBackgroundResource(R.color.profile_offer_accepted);
                offer.acceptPanel.setVisibility(View.GONE);
                break;
            }
            case DECLINED:{
                offer.root.setBackgroundResource(R.color.profile_offer_declined);
                offer.acceptPanel.setVisibility(View.GONE);
                break;
            }
            case COMPLETE:{
                offer.root.setBackgroundResource(R.color.profile_offer_complete);
                offer.acceptPanel.setVisibility(View.GONE);
                break;
            }
        }

        offer.created.setText(data.created.toString("EEE dd. MMMM"));
        offer.amount.setText(String.valueOf(data.amount));
    }

    @Override
    public void onBindData(final RecyclerView.ViewHolder offer, final PQOffer data, int viewType, final int position) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.offer_accept:{

            }
            case R.id.offer_decline:{

            }
            case R.id.profile_option:{
                switch ((int)v.getTag()){
                    case ACTION_CREDIT:{
                        Log.d(TAG, "Credt");
                    }
                    case ACTION_GROUPS:{

                    }
                    case ACTION_POSTS:{

                    }
                }
            }
        }
    }

    @Override
    public boolean itemExist(PQOffer oldItem, PQOffer newItem) {
        return oldItem.id == newItem.id;
    }

    public void removeItemById(int messageId){
        for(int i = 0; i < getItemCount(); i++){
            PQOffer toDelete = getItem(i);
            if(toDelete.id == messageId){
                removeItem(toDelete);
            }
        }
    }
}
