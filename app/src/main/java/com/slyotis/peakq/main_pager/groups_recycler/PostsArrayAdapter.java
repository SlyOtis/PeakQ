package com.slyotis.peakq.main_pager.groups_recycler;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.slyotis.peakq.Chat;
import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerArrayAdapter;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQMessage;
import com.slyotis.peakq.data.PQPost;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQServerOld;
import com.slyotis.peakq.data.PQUser;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

/**
 * Created by slyotis on 14.11.16.
 */

public class PostsArrayAdapter extends RecyclerArrayAdapter<GroupsPost, PQPost> {

    private Activity activity;

    public interface PostExpandListener{
        void onPostExpanded(ViewGroup group);
    }
    public PostsArrayAdapter(Activity activity) {
        this.activity = activity;
    }


    @Override
    public void onBindData(final GroupsPost post, final PQPost data, int viewType, final int position) {

        post.date.setText(data.added.toString("EEE dd. MMM"));
        post.author.setText(data.authorName);
        post.description.setText(data.description);
        post.title.setText(data.title);
        //post.image.setImageBitmap(null);

        Interval interval = new Interval(DateTime.now(), data.expires);
        Period period = interval.toPeriod();

        if(period.getDays() < 1){
            if(period.getHours() <= 0) {
                post.timerType.setText(R.string.groups_post_timer_remaining_minutes);
                post.timer.setText(String.valueOf(period.getMinutes()));
                post.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_minutes);
            } else {
                post.timerType.setText(R.string.groups_post_timer_remaining_hours);
                post.timer.setText(String.valueOf(period.getHours()));
                post.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_hours);
            }
        } else {
            post.timerType.setText(R.string.groups_post_timer_remaining_days);
            post.timer.setText(String.valueOf(period.getDays()));
            if(period.getDays() > 31){
                post.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_months);
            } else if(period.getDays() > 7){
                post.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_weeks);
            } else{
                post.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_days);
            }
        }

        if(data.hasImage){
          //  post.image.setVisibility(View.VISIBLE);
            post.timerContainer.setVisibility(View.GONE);

            ImageLoader loader = new ImageLoader(activity, post);
            loader.execute(data.id);
            /*post.image.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            post.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LayoutInflater inflater = activity.getLayoutInflater();
                    View imageDialog = inflater.inflate(R.layout.image_display, null);
                    Dialog dialog = new Dialog(activity, R.style.DisplayDialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(imageDialog);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DisplayDialogAnimation;

                    SubsamplingScaleImageView imgImage = (SubsamplingScaleImageView) imageDialog.findViewById(R.id.display_image);
                    imgImage.setImage(ImageSource.bitmap(PQDataHandler.getInstance(activity).getImageLarge(data.id)));

                    dialog.show();
                }
            });
*/
        } else {
            //post.image.setVisibility(View.GONE);
            post.timerContainer.setVisibility(View.VISIBLE);
        }

        post.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandedPost.launch(activity, post.root, post, data);

                /*
                if(user.id != data.authorId) {

                    /*
                        Intent intent = new Intent(activity, Chat.class);
                        intent.putExtra(Chat.POST_DATA, data);
                        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        activity.startActivity(intent);

                } else {
                    // TODO : open create post
                    Toast.makeText(activity, "Du kan ikke chatte med deg selv, fixer dette senere",
                            Toast.LENGTH_SHORT).show();
                }
                */
            }
        });
    }

    @Override
    public boolean itemExist(PQPost oldItem, PQPost newItem) {
        if(oldItem.id == newItem.id) return true;
        else return false;
    }

    private class ImageLoader extends AsyncTask<Integer, Integer, Bitmap> {

        private GroupsPost post;
        private ValueAnimator animator;
        private Activity activity;

        public ImageLoader(Activity activity, final GroupsPost post) {
            this.post = post;
            this.activity = activity;
            this.animator = ValueAnimator.ofArgb(Color.MAGENTA, Color.GREEN, Color.RED, Color.YELLOW);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    //post.image.setBackgroundColor((int)animator.getAnimatedValue());
                }
            });
            animator.setDuration(4000);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.setRepeatCount(Animation.INFINITE);
        }

        @Override
        protected void onPreExecute() {
            animator.start();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //if(bitmap != null) post.image.setImageBitmap(bitmap);
            //else post.image.setVisibility(View.GONE);
            animator.end();
            Toast.makeText(activity, R.string.error_loading_image, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Bitmap doInBackground(Integer... params) {
            PQDataHandler db = PQDataHandler.getInstance(activity);
            int id = params[0];
            Bitmap image = db.getImageSmall(id);
            if(image == null){
                PQUser user = db.getCurrentUser();
                byte[] imageData = PQServer.getImage(id, activity);
                if(imageData != null){
                    db.addImage(id, imageData);
                    image = db.getImageSmall(id);
                }
            }
            return image;
        }
    }

    @Override
    public GroupsPost onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsPost(LayoutInflater.from(parent.getContext()).inflate(R.layout.groups_post,
                parent, false));
    }

    public void removeItemById(int messageId){
        for(int i = 0; i < getItemCount(); i++){
            PQPost toDelete = getItem(i);
            if(toDelete.id == messageId){
                removeItem(toDelete);
            }
        }
    }

}
