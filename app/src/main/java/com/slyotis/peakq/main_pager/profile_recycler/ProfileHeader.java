package com.slyotis.peakq.main_pager.profile_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 18.09.16.
 */
@Deprecated
public class ProfileHeader extends RecyclerView.ViewHolder{
    public TextView username;
    public TextView points;

    public ProfileHeader(View itemView) {
        super(itemView);
        username = (TextView)itemView.findViewById(R.id.profile_header_username);
        points = (TextView)itemView.findViewById(R.id.profile_header_credits);
    }
}
