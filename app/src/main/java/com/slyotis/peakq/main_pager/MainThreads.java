package com.slyotis.peakq.main_pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slyotis.peakq.R;
import com.slyotis.peakq.behaviour.DividerItemDecoration;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.data.PQThread;
import com.slyotis.peakq.data.PQUser;
import com.slyotis.peakq.main_pager.messages_recycler.ThreadsArrayAdapter;

/**
 * Created by slyotis on 17.09.16.
 */
public class MainThreads extends PageFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = MainThreads.class.getName();

    private RecyclerView recycler;
    private LinearLayoutManager layoutManager;
    private ThreadsArrayAdapter adapter;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;

    private int defaultThreadCount = 20;
    private int index = 0;

    public static MainThreads newInstance(){
        return new MainThreads();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.main_pager_messages, container, false);

        toolbar = (Toolbar)root.findViewById(R.id.messages_toolbar);
        toolbar.setTitle(R.string.page_messages_title);
        toolbar.setNavigationIcon(R.drawable.ic_mail_white_24dp);

        recycler = (RecyclerView)root.findViewById(R.id.threads_recycler);
        // To increase performance
        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recycler.setLayoutManager(layoutManager);
        adapter = new ThreadsArrayAdapter(getActivity());
        recycler.setAdapter(adapter);

        refreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.threads_refresh);
        refreshLayout.setOnRefreshListener(this);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(adapter.getItemCount() == 0)
            onRefresh();
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        loadThreads(defaultThreadCount, 0);
    }

    private void loadThreads(int count, int start){
        PQServer.getThreads(count, start, threadsCallback, getActivity());
    }

    private PQServer.Callback<PQThread[]> threadsCallback = new PQServer.Callback<PQThread[]>() {
        @Override
        public void onFailure(int errorCode, String message) {
            refreshLayout.setRefreshing(false);
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQThread[] response) {
            refreshLayout.setRefreshing(false);
            adapter.appendItems(response);
        }
    };
}
