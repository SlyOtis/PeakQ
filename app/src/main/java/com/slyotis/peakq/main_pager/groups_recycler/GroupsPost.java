package com.slyotis.peakq.main_pager.groups_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 21.09.16.
 */

public class GroupsPost extends RecyclerView.ViewHolder{
    public TextView timer;
    public TextView date;
    public TextView title;
    public TextView timerType;
    public TextView description;
    public TextView author;
    //public ImageView image;
    public LinearLayout timerContainer;
    public ViewGroup root;

    public GroupsPost(View itemView) {
        super(itemView);
        date = (TextView)itemView.findViewById(R.id.groups_post_date);
        timer = (TextView)itemView.findViewById(R.id.groups_post_timer);
        timerType = (TextView) itemView.findViewById(R.id.groups_post_timer_type);
        title = (TextView)itemView.findViewById(R.id.groups_post_title);
        description = (TextView) itemView.findViewById(R.id.groups_post_description);
        author = (TextView)itemView.findViewById(R.id.groups_post_author);
        //image = (ImageView)itemView.findViewById(R.id.groups_post_image);
        timerContainer = (LinearLayout)itemView.findViewById(R.id.groups_post_timer_container);
        root = (ViewGroup)itemView.findViewById(R.id.groups_post);
    }
}
