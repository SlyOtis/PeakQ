package com.slyotis.peakq.main_pager.messages_recycler;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 22.09.16.
 */
@Deprecated
public class MessagesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    public MessagesRecyclerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessagesThread(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.messages_thread, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessagesThread group = (MessagesThread)holder;
        if(position % 2 == 0)group.root.setBackgroundColor(ContextCompat.getColor(context,
                R.color.messages_thread_background_alt));
        else group.root.setBackgroundColor(ContextCompat.getColor(context,
                R.color.messages_thread_background));
    }

    @Override
    public int getItemCount() {
        return 20;
    }
}
