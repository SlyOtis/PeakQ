package com.slyotis.peakq.main_pager.groups_recycler;

import android.app.Activity;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.custom.RecyclerViewCursorAdapter;
import com.slyotis.peakq.data.PQDataHandler;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.data.PQServerOld;
import com.slyotis.peakq.data.PQUser;

/**
 * Created by slyotis on 21.10.16.
 */
@Deprecated
public class GroupsAdapter extends RecyclerViewCursorAdapter<PQGroup, GroupsGroup> {

    private Activity activity;

    public GroupsAdapter(Activity activity, @NonNull Class<PQGroup> klass, @Nullable Cursor cursor) {
        super(klass, cursor);
        this.activity = activity;
    }

    @Override
    public PQGroup fromCursorRow(Cursor cursor) {
        return PQDataHandler.getInstance(activity).getGroupFromRow(cursor);
    }

    @Override
    public GroupsGroup onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroupsGroup(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.groups_group, parent, false));
    }

    @Override
    public void onBindViewHolder(final GroupsGroup group, int position) {
        final PQGroup data = getItem(position);
        group.title.setText(data.name);
        group.description.setText(data.about);
        group.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        if(data.isMember){
            /*
            group.memberOptions.setVisibility(View.VISIBLE);
            group.notifications.setChecked(data.getPush);
            group.notifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    PQUser user = PQDataHandler.getCurrentUser(activity);
                    PQServerOld.updateMemberships(user.id, user.password, data.id, data.isMember, isChecked, new PQServerOld.Callback() {
                        @Override
                        public void onFailure(int errorCode) {
                            Log.d("ERROR", errorCode + "");
                        }

                        @Override
                        public void onSuccess(int requestId, String response) {
                            PQDataHandler db = PQDataHandler.getInstance(activity);
                            db.updateMembership(response);
                        }
                    }, activity);
                }
            });
            */
        }

        if(position % 2 == 0)group.root.setBackgroundColor(ContextCompat.getColor(activity,
                R.color.profile_post_background_alt));
        else group.root.setBackgroundColor(ContextCompat.getColor(activity,
                R.color.profile_post_background));
    }

}
