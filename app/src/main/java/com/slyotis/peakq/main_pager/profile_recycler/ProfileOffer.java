package com.slyotis.peakq.main_pager.profile_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slyotis.peakq.R;

/**
 * Created by slyotis on 13.11.16.
 */

public class ProfileOffer extends RecyclerView.ViewHolder {
    public TextView created;
    public TextView post;
    public TextView amount;
    public TextView data;
    public TextView timer;
    public TextView timerType;
    public View accept;
    public View decline;
    public View root;
    public View acceptPanel;
    public View timerContainer;

    public ProfileOffer(View itemView) {
        super(itemView);
        root = itemView.findViewById(R.id.offer);
        created = (TextView)itemView.findViewById(R.id.offer_created);
        post = (TextView)itemView.findViewById(R.id.offer_post);
        amount = (TextView)itemView.findViewById(R.id.offer_amount);
        data = (TextView)itemView.findViewById(R.id.offer_data);
        accept = itemView.findViewById(R.id.offer_accept);
        decline = itemView.findViewById(R.id.offer_decline);
        acceptPanel = itemView.findViewById(R.id.offer_accept_panel);
        timerContainer = itemView.findViewById(R.id.offer_timer_container);
        timer = (TextView)itemView.findViewById(R.id.offer_timer);
        timerType = (TextView) itemView.findViewById(R.id.offer_timer_type);
    }
}
