package com.slyotis.peakq.main_pager.groups_recycler;

import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.slyotis.peakq.R;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.firebase.FBGroup;
import com.slyotis.peakq.firebase.FBUserGroup;
import com.slyotis.peakq.firebase.PQFire;

import org.joda.time.DateTime;

/**
 * Created by slyotis on 06.12.16.
 */

public class FBGroupsAdapter extends FirebaseRecyclerAdapter<FBUserGroup, GroupsGroup> {

    private RecyclerView recyclerView;
    private int expandedPosition = -1;
    private String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

    private OnGroupSelectedListener onGroupSelectedListener;

    public interface OnGroupSelectedListener{
        void onGroupSelected(FBUserGroup group);
    }

    public FBGroupsAdapter(OnGroupSelectedListener listener) {
        super(FBUserGroup.class, R.layout.groups_group, GroupsGroup.class,
                FirebaseDatabase.getInstance().getReference(PQFire.TABLE_GROUPS));
        this.onGroupSelectedListener = listener;

    }

    @Override
    protected void populateViewHolder(GroupsGroup group, FBUserGroup model, final int position) {
        FBUserGroup data = getItem(position);
        group.title.setText(data.name);
        group.description.setText(data.about);
        group.date.setText(DateTime.parse(data.created, PQFire.defaultFormat).toString("EEEE dd. MMMM"));

        final boolean isExpanded = expandedPosition == position;
        group.showOptionsFull(isExpanded);
        group.more.setActivated(isExpanded);

        group.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandedPosition = isExpanded ? -1:position;
                TransitionManager.beginDelayedTransition(recyclerView,
                        TransitionInflater.from(recyclerView.getContext()).inflateTransition(R.transition.group_options));
                notifyDataSetChanged();
            }
        });

        FirebaseDatabase.getInstance().getReference(
                PQFire.TABLE_USERS + "/" + userId + "/" + PQFire.TABLE_GROUPS + "/" + model.id)
                .addListenerForSingleValueEvent(new GroupMembershipListener(group));
    }

    public void updateItemById(String id){
        for(int i = 0 ; i < getItemCount(); i++){
            if(getItem(i).id == id){

            }
        }
    }

    public class GroupMembershipListener implements ValueEventListener{

        GroupsGroup holder;

        public GroupMembershipListener(GroupsGroup holder){
            this.holder = holder;
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            FBUserGroup group = dataSnapshot.getValue(FBUserGroup.class);
            // No membership found
            if(group != null){
                if(group.isAdmin){
                    holder.isAdmin(true);
                    holder.setIsMember(true);
                    holder.setPushOn(group.getPush);
                    bindMemberOptions(holder, group);
                    bindAdminOptions(holder, group);
                } else if(group.isAdmin) {
                    holder.isMember(true);
                    holder.setIsMember(true);
                    holder.setPushOn(group.getPush);
                    bindMemberOptions(holder, group);
                } else {
                    holder.isMember(false);
                    holder.setIsMember(false);
                    holder.setPushOn(false);
                    View.OnClickListener join = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //TODO:: Fix this
                            FirebaseDatabase.getInstance().getReference(PQFire.TABLE_USERS);
                            //   PQServer.joinGroup(data.id, false, joinGroupCallback, activity);
                        }
                    };
                    holder.memberBtn.setOnClickListener(join);
                    holder.member.setOnClickListener(join);
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }

        public void bindMemberOptions(GroupsGroup group, final FBUserGroup data){
            View.OnClickListener push = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*PQServer.updateMembership(data.id, !data.getPush,
                        updatePushCallback, activity); */
                }
            };

            group.push.setOnClickListener(push);
            group.pushBtn.setOnClickListener(push);

            View.OnClickListener leave = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                /*PQServer.leaveGroup(data.id, leaveGroupCallback, activity); */
                }
            };

            group.member.setOnClickListener(leave);
            group.memberBtn.setOnClickListener(leave);

            group.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGroupSelectedListener.onGroupSelected(data);
                }
            });
        }

        public void bindAdminOptions(GroupsGroup group, FBUserGroup data){
            View.OnClickListener edit = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(recyclerView.getContext(), R.string.error_comming_soon, Toast.LENGTH_SHORT).show();
                }
            };

            group.edit.setOnClickListener(edit);
            group.editBtn.setOnClickListener(edit);
        }

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    public void bindMemberOptions(GroupsGroup group, final FBUserGroup data){
        View.OnClickListener push = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PQServer.updateMembership(data.id, !data.getPush,
                        updatePushCallback, activity); */
            }
        };

        group.push.setOnClickListener(push);
        group.pushBtn.setOnClickListener(push);

        View.OnClickListener leave = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PQServer.leaveGroup(data.id, leaveGroupCallback, activity); */
            }
        };

        group.member.setOnClickListener(leave);
        group.memberBtn.setOnClickListener(leave);

        group.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onGroupSelectedListener.onGroupSelected(data);
            }
        });
    }

    public void bindAdminOptions(GroupsGroup group, FBUserGroup data){
        View.OnClickListener edit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity, R.string.error_comming_soon, Toast.LENGTH_SHORT).show();
            }
        };

        group.edit.setOnClickListener(edit);
        group.editBtn.setOnClickListener(edit);
    }
}
