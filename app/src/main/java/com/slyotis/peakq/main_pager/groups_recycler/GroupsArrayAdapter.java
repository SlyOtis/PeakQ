package com.slyotis.peakq.main_pager.groups_recycler;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerArrayAdapter;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.data.PQInfo;
import com.slyotis.peakq.data.PQMembership;
import com.slyotis.peakq.data.PQServer;
import com.slyotis.peakq.firebase.FBGroup;
import com.slyotis.peakq.firebase.PQFire;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by slyotis on 13.11.16.
 */

public class GroupsArrayAdapter extends RecyclerArrayAdapter<RecyclerView.ViewHolder, PQGroup> {

    private final static String TAG = GroupsArrayAdapter.class.getName();

    private final static int VIEW_ITEM = 0;
    private final static int VIEW_PROG = 1;

    private Activity activity;
    private List<PQGroup> groups;
    private OnGroupSelectedListener onGroupSelectedListener;
    private RecyclerView recyclerView;

    private int expandedPosition = -1;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    boolean loading = false;

    public interface OnGroupSelectedListener{
        void onGroupSelected(PQGroup group);
    }

    public GroupsArrayAdapter(Activity activity, OnGroupSelectedListener listener, RecyclerView recyclerView) {
        this.activity = activity;
        this.onGroupSelectedListener = listener;
        this.groups = new ArrayList<>();
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case VIEW_ITEM:
                return new GroupsGroup(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.groups_group, parent, false));
            case VIEW_PROG:
                return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.adapter_loader, parent, false));
        }
        return null;
    }

    public void bindMemberOptions(GroupsGroup group, final PQGroup data){
        View.OnClickListener push = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PQServer.updateMembership(data.id, !data.getPush,
                        updatePushCallback, activity); */
            }
        };

        group.push.setOnClickListener(push);
        group.pushBtn.setOnClickListener(push);

        View.OnClickListener leave = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*PQServer.leaveGroup(data.id, leaveGroupCallback, activity); */
            }
        };

        group.member.setOnClickListener(leave);
        group.memberBtn.setOnClickListener(leave);

        group.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onGroupSelectedListener.onGroupSelected(data);
            }
        });
    }

    public void bindAdminOptions(GroupsGroup group, PQGroup data){
        View.OnClickListener edit = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, R.string.error_comming_soon, Toast.LENGTH_SHORT).show();
            }
        };

        group.edit.setOnClickListener(edit);
        group.editBtn.setOnClickListener(edit);
    }
    @Override
    public void onBindData(final RecyclerView.ViewHolder holder, final PQGroup data, int viewType, final int position) {
        if(viewType == VIEW_PROG) return;

        GroupsGroup group = (GroupsGroup) holder;
        group.title.setText(data.name);
        group.description.setText(data.about);
        //group.date.setText(DateTime.parse(data.created, PQFire.defaultFormat).toString("EEEE dd. MMMM"));

        final boolean isExpanded = expandedPosition == position;
        group.showOptionsFull(isExpanded);
        group.more.setActivated(isExpanded);

        group.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int oldPos = expandedPosition;
                expandedPosition = isExpanded ? -1:position;
                TransitionManager.beginDelayedTransition(recyclerView,
                        TransitionInflater.from(activity).inflateTransition(R.transition.group_options));
                notifyDataSetChanged();
            }
        });

        if(data.isAdmin){
            group.isAdmin(true);
            group.setIsMember(true);
            group.setPushOn(data.getPush);
            bindMemberOptions(group, data);
            bindAdminOptions(group, data);
        } else if(data.isAdmin) {
            group.isMember(true);
            group.setIsMember(true);
            group.setPushOn(data.getPush);
            bindMemberOptions(group, data);
        } else {
            group.isMember(false);
            group.setIsMember(false);
            group.setPushOn(false);
            View.OnClickListener join = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                 //   PQServer.joinGroup(data.id, false, joinGroupCallback, activity);
                }
            };
            group.memberBtn.setOnClickListener(join);
            group.member.setOnClickListener(join);
        }
    }

    @Override
    public boolean itemExist(PQGroup oldItem, PQGroup newItem) {
        return oldItem.id == newItem.id;
    }

    private PQServer.Callback<PQMembership> updatePushCallback = new PQServer.Callback<PQMembership>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQMembership response) {
            Log.d(TAG, " Group id " + response.refId);
            /*for(int i = 0; i < getItemCount(); i++){
                FB item = getItem(i);
                if(item.id == response.refId){
                    item.getPush = response.getPush;
                    updateItem(i, item);
                }
            } */
        }
    };

    private PQServer.Callback<PQInfo> joinGroupCallback = new PQServer.Callback<PQInfo>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQInfo response) {
            for(int i = 0; i < getItemCount(); i++){
               /* PQGroup item = getItem(i);
                if(item.id == response.id){
                    item.isMember = true;
                    updateItem(i, item);
                } */
            }
        }
    };

    private PQServer.Callback<PQInfo> leaveGroupCallback = new PQServer.Callback<PQInfo>() {
        @Override
        public void onFailure(int errorCode, String message) {
            Log.d(TAG, errorCode + ", " + message);
        }

        @Override
        public void onSuccess(int requestId, PQInfo response) {
            for(int i = 0; i < getItemCount(); i++){
               /* PQGroup item = getItem(i);
                if(item.id == response.id){
                    item.isAdmin = false;
                    item.isMember = false;
                    updateItem(i, item);
                } */
            }
        }
    };

    public PQGroup getItemById(int id){
        for(int i = 0; i < getItemCount(); i++){
           /* PQGroup item = getItem(i);
            if(item.id == id){
                return item;
            } */
        }
        return null;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View itemView) {
            super(itemView);
            final View loader = itemView.findViewById(R.id.adapter_loader);
            final ValueAnimator animator = ValueAnimator.ofArgb(Color.MAGENTA, Color.GREEN, Color.RED, Color.YELLOW);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    loader.setBackgroundColor((int)animator.getAnimatedValue());
                }
            });
            animator.setDuration(4000);
            animator.setRepeatMode(ValueAnimator.REVERSE);
            animator.setRepeatCount(Animation.INFINITE);
        }
    }
}
