package com.slyotis.peakq.main_pager.groups_recycler;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.FirebaseDatabase;
import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.custom.SmoothLinearLayoutManager;
import com.slyotis.peakq.dialogs.CreateGroup;
import com.slyotis.peakq.firebase.FBUserGroup;
import com.slyotis.peakq.firebase.PQFire;
import com.slyotis.peakq.main_pager.MainGroups;
import com.slyotis.peakq.views.FabulousActionButton;

/**
 * Created by slyotis on 24.10.16.
 */

public class GroupsGroups extends PageFragment implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener{
    // FOr debugging purposes
    private static final String TAG = MainGroups.class.getName();
    private static final int CREATE_GROUP = 1;

    public static final String CREATE_GROUP_DATA = "create_group_data";

    private RecyclerView recycler;
    private SmoothLinearLayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;
    private FabulousActionButton fab;

    private FBGroupsAdapter adapter;

    private int defaultGroupCount = 20;
    private int index = 0;

    // TODO: Do something with this
    private boolean isMember;
    private boolean loading;

    private int visibleThreshold = 2;
    private int totalItemCount, lastVisibleItem;

    public static GroupsGroups newInstance(){
        GroupsGroups groupsGroup = new GroupsGroups();
        return groupsGroup;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.groups_groups, container, false);

        refreshLayout = (SwipeRefreshLayout)root.findViewById(R.id.groups_refresh);

        recycler = (RecyclerView)root.findViewById(R.id.groups_recycler);
        recycler.setHasFixedSize(true);
        layoutManager = new SmoothLinearLayoutManager(getActivity());
        recycler.setLayoutManager(layoutManager);

        adapter = new FBGroupsAdapter(null);
        recycler.setAdapter(adapter);

        fab = (FabulousActionButton) root.findViewById(R.id.groups_fab);
        fab.setOnClickListener(this);

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    // Do something
                    loading = true;
                }
            }
        });

        return root;
    }

    @Override
    public void onClick(View v) {
        CreateGroup dialog = CreateGroup.newInstance();
        dialog.show(getActivity().getFragmentManager(), "create_group");
        /*
        Intent intent = new Intent(getActivity(), CreateGroup.class);
        startActivityForResult(intent, CREATE_GROUP);
        */
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onRefresh() {

    }
}
