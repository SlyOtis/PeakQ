package com.slyotis.peakq.main_pager.messages_recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.slyotis.peakq.R;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

/**
 * Created by slyotis on 22.09.16.
 */

public class MessagesThread extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView date;
    public TextView post;
    public TextView lastMessage;
    public TextView timer;
    public TextView timerType;
    public View timerContainer;
    public View root;

    public MessagesThread(View itemView) {
        super(itemView);
        date = (TextView)itemView.findViewById(R.id.thread_date);
        title = (TextView)itemView.findViewById(R.id.thread_title);
        post = (TextView)itemView.findViewById(R.id.thread_post_title);
        lastMessage = (TextView)itemView.findViewById(R.id.thread_last_message);
        root = itemView.findViewById(R.id.messages_thread);
        timer = (TextView) itemView.findViewById(R.id.thread_timer);
        timerType = (TextView) itemView.findViewById(R.id.thread_timer_type);
        timerContainer = itemView.findViewById(R.id.thread_timer_container);
    }

    public void setExpires(DateTime expires){
        Interval interval = new Interval(DateTime.now(), expires);
        Period period = interval.toPeriod();

        if(period.getDays() < 1){
            if(period.getHours() <= 0) {
                timerType.setText(R.string.groups_post_timer_remaining_minutes);
                timer.setText(String.valueOf(period.getMinutes()));
                timerContainer.setBackgroundResource(R.color.groups_post_timer_background_minutes);
            } else {
                timerType.setText(R.string.groups_post_timer_remaining_hours);
                timer.setText(String.valueOf(period.getHours()));
                timerContainer.setBackgroundResource(R.color.groups_post_timer_background_hours);
            }
        } else {
            timerType.setText(R.string.groups_post_timer_remaining_days);
            timer.setText(String.valueOf(period.getDays()));
            if(period.getDays() > 31){
                timerContainer.setBackgroundResource(R.color.groups_post_timer_background_months);
            } else if(period.getDays() > 7){
                timerContainer.setBackgroundResource(R.color.groups_post_timer_background_weeks);
            } else{
                timerContainer.setBackgroundResource(R.color.groups_post_timer_background_days);
            }
        }
    }
}
