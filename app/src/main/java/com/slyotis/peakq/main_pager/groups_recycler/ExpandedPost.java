package com.slyotis.peakq.main_pager.groups_recycler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.PaintDrawable;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.slyotis.peakq.R;
import com.slyotis.peakq.data.PQPost;

/**
 * Created by slyotis on 04.12.16.
 */

public class ExpandedPost extends LinearLayout {

    public View timerContainer;
    public TextView timer;
    public TextView timerType;
    public TextView date;
    public TextView title;
    public TextView author;
    public TextView description;


    public ExpandedPost(Context context) {
        super(context);
    }

    public ExpandedPost(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandedPost(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExpandedPost(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init(){

    }



    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        timerContainer = findViewById(R.id.groups_post_timer_container);
        timer = (TextView)findViewById(R.id.groups_post_timer);
        timerType = (TextView) findViewById(R.id.groups_post_timer_type);
        date = (TextView) findViewById(R.id.groups_post_date);
        title = (TextView) findViewById(R.id.groups_post_title);
        author = (TextView) findViewById(R.id.groups_post_author);
        description = (TextView) findViewById(R.id.groups_post_description);
    }

    public static void launch(Activity activity, final ViewGroup container, GroupsPost iv, PQPost post){
        final ExpandedPost v = (ExpandedPost) activity.getLayoutInflater().inflate(R.layout.groups_post_expanded, container, false);
        v.timerContainer.setBackgroundColor(((ColorDrawable)iv.timerContainer.getBackground()).getColor());
        v.timer.setText(iv.timer.getText());
        v.timerType.setText(iv.timerType.getText());
        v.author.setText(iv.author.getText());
        v.date.setText(iv.date.getText());
        v.description.setText(iv.description.getText());
        Transition shared = TransitionInflater.from(activity).inflateTransition(android.R.transition.move);
        shared.addTarget("timer_container");
        shared.addTarget("timer");
        shared.addTarget("timer_type");
        shared.addTarget("date");
        shared.addTarget("description");
        shared.addTarget("title");
        shared.addTarget("author");
        shared.setDuration(400);
        Transition fade = TransitionInflater.from(activity).inflateTransition(android.R.transition.fade);
        fade.excludeTarget("timer_container", true);
        fade.excludeTarget("timer", true);
        fade.excludeTarget("timer_type", true);
        fade.excludeTarget("date", true);
        fade.excludeTarget("description", true);
        fade.excludeTarget("title", true);
        fade.excludeTarget("author", true);
        fade.setDuration(400);
        final TransitionSet set = new TransitionSet();
        //set.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        set.addTransition(fade).addTransition(shared);
        //set.addTransition(shared).addTransition(fade);
        Scene scene = new Scene(container, (View)v);
        TransitionManager.go(scene, set);
    }
}
