package com.slyotis.peakq.main_pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.PageFragment;
import com.slyotis.peakq.data.PQGroup;
import com.slyotis.peakq.firebase.FBUserGroup;
import com.slyotis.peakq.main_pager.groups_recycler.GroupsGroups;
import com.slyotis.peakq.main_pager.groups_recycler.GroupsPosts;

/**
 * Created by slyotis on 17.09.16.
 */
public class MainGroups extends PageFragment{
    // FOr debugging purposes
    private static final String TAG = MainGroups.class.getName();

    private GroupsGroups groupsFragment;
    private Toolbar toolbar;

    private boolean inGroup;
    private PQGroup group;

    public static MainGroups newInstance(){
        return new MainGroups();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.main_pager_groups, container, false);

        toolbar = (Toolbar)root.findViewById(R.id.groups_toolbar);
        toolbar.setTitle(R.string.page_groups_title);
        toolbar.setSubtitle(null);
        toolbar.setNavigationIcon(R.drawable.ic_group_white_24dp);
        toolbar.setNavigationOnClickListener(null);

        groupsFragment = GroupsGroups.newInstance();
        getFragmentManager().beginTransaction()
                .add(R.id.groups_fragment, groupsFragment)
                .commit();

        return root;
    }



    public void initializeSearch(){

    }

    public boolean isInGroup(){
        return inGroup;
    }

    public PQGroup getCurrentGroup(){
        return group;
    }

    public void showGroupsList(){
        inGroup = false;
        groupsFragment = GroupsGroups.newInstance();
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.groups_fragment, groupsFragment)
                .commit();

        toolbar.setTitle(R.string.page_groups_title);
        toolbar.setSubtitle(null);
        toolbar.setNavigationIcon(R.drawable.ic_group_white_24dp);
        toolbar.setNavigationOnClickListener(null);
    }

    public void onGroupSelected(FBUserGroup group) {
        groupsFragment = null;
        inGroup = true;

        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(R.id.groups_fragment, GroupsPosts.newInstance(null))
                .commit();

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle(group.name);
        toolbar.setSubtitle(group.about);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGroupsList();
            }
        });
    }
}
