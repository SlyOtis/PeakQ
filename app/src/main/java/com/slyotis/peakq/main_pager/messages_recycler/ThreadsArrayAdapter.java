package com.slyotis.peakq.main_pager.messages_recycler;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.slyotis.peakq.Chat;
import com.slyotis.peakq.R;
import com.slyotis.peakq.custom.RecyclerArrayAdapter;
import com.slyotis.peakq.data.PQThread;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;

/**
 * Created by slyotis on 14.11.16.
 */

public class ThreadsArrayAdapter extends RecyclerArrayAdapter<MessagesThread, PQThread> {

    private Activity activity;
    public ThreadsArrayAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onBindData(MessagesThread holder, final PQThread data, int viewType, int position) {
        holder.date.setText(data.created.toString("EEE dd. MMM"));
        holder.post.setText(data.postTitle);
        // TODO: Add last message to database

        Interval interval = new Interval(DateTime.now(), data.expires);
        Period period = interval.toPeriod();

        if(period.getDays() < 1){
            if(period.getHours() <= 0) {
                holder.timerType.setText(R.string.groups_post_timer_remaining_minutes);
                holder.timer.setText(String.valueOf(period.getMinutes()));
                holder.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_minutes);
            } else {
                holder.timerType.setText(R.string.groups_post_timer_remaining_hours);
                holder.timer.setText(String.valueOf(period.getHours()));
                holder.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_hours);
            }
        } else {
            holder.timerType.setText(R.string.groups_post_timer_remaining_days);
            holder.timer.setText(String.valueOf(period.getDays()));
            if(period.getDays() > 31){
                holder.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_months);
            } else if(period.getDays() > 7){
                holder.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_weeks);
            } else{
                holder.timerContainer.setBackgroundResource(R.color.groups_post_timer_background_days);
            }
        }

        holder.lastMessage.setText(data.postTitle);
        holder.title.setText(data.title);

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Chat.class);
                intent.putExtra(Chat.CHAT_THREAD, data);
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public boolean itemExist(PQThread oldItem, PQThread newItem) {
        return oldItem.id == newItem.id;
    }

    @Override
    public MessagesThread onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessagesThread(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.messages_thread, parent, false));
    }
}
